package deprecated.polcz.mysql.model;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(ProductInfo.class)
public abstract class ProductInfo_ {

	public static volatile SingularAttribute<ProductInfo, Integer> piId;
	public static volatile SingularAttribute<ProductInfo, Integer> piAmount;
	public static volatile SingularAttribute<ProductInfo, Market> market;
	public static volatile SingularAttribute<ProductInfo, Date> piDate;
	public static volatile SingularAttribute<ProductInfo, Cluster> cluster;
	public static volatile SingularAttribute<ProductInfo, String> piWhat;
	public static volatile SingularAttribute<ProductInfo, ChargeAccount> chargeAccount;
	public static volatile SingularAttribute<ProductInfo, Transaction> transaction;

}

