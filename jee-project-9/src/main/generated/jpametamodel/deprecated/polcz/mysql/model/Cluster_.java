package deprecated.polcz.mysql.model;

import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Cluster.class)
public abstract class Cluster_ {

	public static volatile SingularAttribute<Cluster, Integer> uid;
	public static volatile SingularAttribute<Cluster, String> parent;
	public static volatile ListAttribute<Cluster, ProductInfo> productInfos;
	public static volatile SingularAttribute<Cluster, String> name;
	public static volatile SingularAttribute<Cluster, Integer> dir;
	public static volatile ListAttribute<Cluster, Transaction> transactions;
	public static volatile SingularAttribute<Cluster, String> desc;

}

