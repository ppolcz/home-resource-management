package deprecated.polcz.mysql.model;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Transaction.class)
public abstract class Transaction_ {

	public static volatile SingularAttribute<Transaction, Boolean> trPivot;
	public static volatile SingularAttribute<Transaction, String> trRemark;
	public static volatile SingularAttribute<Transaction, Market> market;
	public static volatile SingularAttribute<Transaction, Integer> trNewbalance;
	public static volatile SingularAttribute<Transaction, Integer> trTmpNewbalance;
	public static volatile SingularAttribute<Transaction, Cluster> cluster;
	public static volatile SingularAttribute<Transaction, Date> trDate;
	public static volatile ListAttribute<Transaction, ProductInfo> productInfos;
	public static volatile SingularAttribute<Transaction, ChargeAccount> chargeAccount;
	public static volatile SingularAttribute<Transaction, Integer> trAmount;
	public static volatile SingularAttribute<Transaction, String> trRemarkExtra;
	public static volatile SingularAttribute<Transaction, Integer> trId;

}

