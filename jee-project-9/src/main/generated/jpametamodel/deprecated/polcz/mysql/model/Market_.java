package deprecated.polcz.mysql.model;

import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Market.class)
public abstract class Market_ {

	public static volatile SingularAttribute<Market, String> mkName;
	public static volatile ListAttribute<Market, ProductInfo> productInfos;
	public static volatile SingularAttribute<Market, String> mkId;
	public static volatile ListAttribute<Market, Transaction> transactions;

}

