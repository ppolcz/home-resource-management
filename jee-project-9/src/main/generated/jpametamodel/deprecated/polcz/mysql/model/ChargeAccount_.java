package deprecated.polcz.mysql.model;

import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(ChargeAccount.class)
public abstract class ChargeAccount_ {

	public static volatile SingularAttribute<ChargeAccount, String> caName;
	public static volatile ListAttribute<ChargeAccount, ProductInfo> productInfos;
	public static volatile SingularAttribute<ChargeAccount, String> caId;
	public static volatile ListAttribute<ChargeAccount, Transaction> transactions;

}

