package polcz.mysql.model.uid;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(TTransaction.class)
public abstract class TTransaction_ {

	public static volatile SingularAttribute<TTransaction, Date> date;
	public static volatile SingularAttribute<TTransaction, TMarket> market;
	public static volatile SingularAttribute<TTransaction, Integer> uid;
	public static volatile SingularAttribute<TTransaction, TCluster> cluster;
	public static volatile SingularAttribute<TTransaction, Integer> amount;
	public static volatile ListAttribute<TTransaction, TProductInfo> TProductInfos;
	public static volatile SingularAttribute<TTransaction, Integer> balance;
	public static volatile SingularAttribute<TTransaction, Boolean> pivot;
	public static volatile SingularAttribute<TTransaction, String> remark;
	public static volatile SingularAttribute<TTransaction, TChargeAccount> ca;

}

