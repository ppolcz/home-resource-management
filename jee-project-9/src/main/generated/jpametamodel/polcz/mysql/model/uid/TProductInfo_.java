package polcz.mysql.model.uid;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(TProductInfo.class)
public abstract class TProductInfo_ {

	public static volatile SingularAttribute<TProductInfo, TCluster> TCluster;
	public static volatile SingularAttribute<TProductInfo, Integer> uid;
	public static volatile SingularAttribute<TProductInfo, Integer> amount;
	public static volatile SingularAttribute<TProductInfo, Integer> amountOrig;
	public static volatile SingularAttribute<TProductInfo, String> currency;
	public static volatile SingularAttribute<TProductInfo, TMarket> TMarket;
	public static volatile SingularAttribute<TProductInfo, TTransaction> TTransaction;
	public static volatile SingularAttribute<TProductInfo, String> desc;

}

