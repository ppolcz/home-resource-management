package polcz.mysql.model.uid;

import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(TCluster.class)
public abstract class TCluster_ {

	public static volatile SingularAttribute<TCluster, Integer> uid;
	public static volatile SingularAttribute<TCluster, TCluster> parent;
	public static volatile ListAttribute<TCluster, TTransaction> trs;
	public static volatile ListAttribute<TCluster, TCluster> children;
	public static volatile SingularAttribute<TCluster, String> name;
	public static volatile SingularAttribute<TCluster, Integer> sgn;
	public static volatile ListAttribute<TCluster, TProductInfo> pis;

}

