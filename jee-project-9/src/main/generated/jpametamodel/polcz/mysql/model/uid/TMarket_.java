package polcz.mysql.model.uid;

import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(TMarket.class)
public abstract class TMarket_ {

	public static volatile SingularAttribute<TMarket, Integer> uid;
	public static volatile ListAttribute<TMarket, TProductInfo> TProductInfos;
	public static volatile ListAttribute<TMarket, TTransaction> trs;
	public static volatile SingularAttribute<TMarket, String> name;
	public static volatile SingularAttribute<TMarket, String> desc;

}

