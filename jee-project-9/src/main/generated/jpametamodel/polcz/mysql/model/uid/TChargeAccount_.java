package polcz.mysql.model.uid;

import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(TChargeAccount.class)
public abstract class TChargeAccount_ {

	public static volatile SingularAttribute<TChargeAccount, Integer> uid;
	public static volatile SingularAttribute<TChargeAccount, String> name;
	public static volatile ListAttribute<TChargeAccount, TTransaction> TTransactions;
	public static volatile SingularAttribute<TChargeAccount, String> desc;

}

