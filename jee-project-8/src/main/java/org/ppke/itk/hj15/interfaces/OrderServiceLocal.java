package org.ppke.itk.hj15.interfaces;

import java.util.List;

import javax.ejb.Local;

import org.ppke.itk.hj15.model.Order;
import org.ppke.itk.hj15.model.Product;

@Local
public interface OrderServiceLocal {
	
	public void addOrder(String userName,String firstName, String lastName, List<Product> products); 
	public List<Order> getOrders(String userName);
	List<Order> getOrders();

}
