
create table if not exists accounts ( 
    ac_username varchar(50) unique
) default character set = utf8;

create table if not exists charge_accounts ( 
    ca_id varchar(8) primary key not null unique,
    ca_name varchar(20),
    ca_date date,
    ca_balance integer
) default character set = utf8;

create table if not exists clusters ( 
    cl_name varchar(20) primary key not null unique,
    cl_description varchar(50),
    cl_direction integer
) default character set = utf8;

create table if not exists markets ( 
    mk_id varchar(32) primary key not null,
    mk_name varchar(20)
) default character set = utf8;

create table if not exists tranzactions ( 
    tr_id integer primary key auto_increment not null unique,
    tr_date date,
    tr_caid varchar(8),
    tr_amount integer,
    tr_newbalance integer,
    tr_clname varchar(20),
    tr_mkname varchar(32),
    tr_remark varchar(128),
    tr_remark_extra varchar(128),
    tr_pivot boolean default false,
    foreign key (tr_caid) references charge_accounts(ca_id),
    foreign key (tr_clname) references clusters(cl_name),
    foreign key (tr_mkname) references markets(mk_id)
) default character set = utf8;

create table if not exists product_info ( 
    pi_id integer primary key auto_increment not null unique,
    pi_trid integer,
    pi_date date,
    pi_amount integer,
    pi_what varchar(50),
    pi_mkid varchar(32),
    pi_clname varchar(20),
    pi_caid varchar(8),
    foreign key (pi_trid) references tranzactions(tr_id),
    foreign key (pi_mkid) references markets(mk_id),
    foreign key (pi_clname) references clusters(cl_name),
    foreign key (pi_caid) references charge_accounts(ca_id)
) default character set = utf8;

SELECT * FROM tranzactions WHERE tr_date > "2015-03-24" AND NOT tr_pivot;

-- ez jol mukodik
DROP PROCEDURE IF EXISTS get_last_transaction;
DELIMITER $$
CREATE PROCEDURE get_last_transaction(IN in_tr_date DATE, IN in_tr_caid VARCHAR(8))
SQL SECURITY DEFINER
BEGIN
    SELECT * FROM tranzactions WHERE tr_date < in_tr_date AND tr_caid = in_tr_caid ORDER BY tr_date DESC LIMIT 1;
END;
$$
DELIMITER ;

-- triggerek
DROP TRIGGER IF EXISTS `tranzactions_before_insert_trigger`;

DELIMITER $$
CREATE DEFINER=`root`@`localhost` TRIGGER `tranzactions_before_insert_trigger` 
BEFORE INSERT ON `tranzactions` FOR EACH ROW 
BEGIN
    DECLARE sign INTEGER;
    DECLARE balance INTEGER;
    DECLARE amount INTEGER;

    SELECT cl_direction FROM clusters WHERE NEW.tr_clname = cl_name INTO sign;
    SELECT tr_newbalance FROM tranzactions WHERE tr_date <= NEW.tr_date AND tr_caid = NEW.tr_caid ORDER BY tr_date DESC, tr_id DESC LIMIT 1 INTO balance;

    SET amount = sign * NEW.tr_amount;
    SET NEW.tr_newbalance = balance + amount;
END; 
$$

DELIMITER ;

-- triggerek
DROP TRIGGER IF EXISTS `tranzactions_after_insert_trigger`;

DELIMITER $$
CREATE DEFINER=`root`@`localhost` TRIGGER `tranzactions_after_insert_trigger` 
AFTER INSERT ON `tranzactions` FOR EACH ROW 
BEGIN
    DECLARE sign INTEGER;
    DECLARE balance INTEGER;
    DECLARE amount INTEGER;

    SELECT cl_direction FROM clusters WHERE NEW.tr_clname = cl_name INTO sign;
    SELECT tr_newbalance FROM tranzactions WHERE tr_date <= NEW.tr_date AND tr_caid = NEW.tr_caid ORDER BY tr_date DESC, tr_id DESC LIMIT 1 INTO balance;

    SET amount = sign * NEW.tr_amount;
    -- SET NEW.tr_newbalance = balance + amount;

    -- UPDATE tranzactions SET tr_newbalance = tr_newbalance + amount WHERE tr_date > NEW.tr_date AND tr_pivot = FALSE;
    -- UPDATE tranzactions SET tr_amount = tr_amount - amount WHERE tr_date > NEW.tr_date AND tr_pivot = TRUE;
END; 
$$

DELIMITER ;

INSERT INTO `javadrafts2`.`tranzactions` 
    (`tr_id`, `tr_date`, `tr_caid`, `tr_amount`, `tr_newbalance`, `tr_clname`, `tr_mkname`, `tr_remark`, `tr_remark_extra`, `tr_pivot`) 
VALUES 
    (NULL, '2015-03-24', 'pkez', '1234', NULL, 'Elektr_Cikk', 'Interspar', NULL, NULL, '0');

SELECT * FROM tranzactions WHERE tr_date <= '2015-03-24' AND tr_caid = 'pkez' ORDER BY tr_date DESC, tr_id DESC LIMIT 1;


DELIMITER $$
START TRANSACTION ;
    -- DECLARE sign INTEGER;
    -- DECLARE balance INTEGER;
    -- DECLARE amount INTEGER;
    DECLARE NEW_tr_clname VARCHAR(45);
    -- DECLARE NEW_tr_date DATE;
    -- DECLARE NEW_tr_id INTEGER;

    INSERT INTO `javadrafts2`.`tranzactions` 
        (`tr_id`, `tr_date`, `tr_caid`, `tr_amount`, `tr_newbalance`, `tr_clname`, `tr_mkname`, `tr_remark`, `tr_remark_extra`, `tr_pivot`) 
    VALUES 
        (NULL, '2015-03-24', 'pkez', '1234', NULL, 'Elektr_Cikk', 'Interspar', NULL, NULL, '0');

    -- SELECT tr_id, tr_date, tr_clname from tranzactions WHERE tr_id = LAST_INSERT_ID() INTO NEW_tr_id, NEW_tr_date, NEW_tr_clname;
    -- SELECT cl_direction FROM clusters WHERE NEW_tr_clname = cl_name INTO sign;

    -- SELECT tr_newbalance FROM tranzactions 
    --     WHERE tr_caid = NEW.tr_caid AND tr_date <= NEW.tr_date AND tr_id < NEW_tr_id 
    --     ORDER BY tr_date DESC, tr_id DESC 
    --     LIMIT 1 
    --     INTO balance;

    -- SET amount = sign * NEW_tr_amount;

    -- UPDATE tranzactions SET tr_newbalance = tr_newbalance + amount WHERE tr_date >= NEW.tr_date AND tr_pivot = FALSE;
    -- UPDATE tranzactions SET tr_amount = tr_amount - amount WHERE tr_date > NEW.tr_date AND tr_pivot = TRUE;
COMMIT $$

DELIMITER ;

-- generate create table command using the existing table
show create table tranzactions;

-- DECLARE sign INTEGER;
-- DECLARE balance INTEGER;
-- DECLARE amount INTEGER;
-- DECLARE NEW_tr_clname VARCHAR(45);
-- DECLARE NEW_tr_date DATE;
-- DECLARE NEW_tr_id INTEGER;

INSERT INTO `javadrafts2`.`tranzactions` 
    (`tr_id`, `tr_date`, `tr_caid`, `tr_amount`, `tr_newbalance`, `tr_clname`, `tr_mkname`, `tr_remark`, `tr_remark_extra`, `tr_pivot`) 
VALUES 
    (NULL, '2015-01-23', 'pkez', '1234', NULL, 'Elektr_Cikk', 'Interspar', NULL, "EXTRA_BETET", '0');

SELECT tr_id, tr_date, tr_clname, tr_amount, tr_caid from tranzactions WHERE tr_id = LAST_INSERT_ID() 
INTO @NEW_tr_id, @NEW_tr_date, @NEW_tr_clname, @NEW_tr_amount, @NEW_tr_caid;

SELECT cl_direction FROM clusters WHERE @NEW_tr_clname = cl_name INTO @sign;

SELECT tr_newbalance FROM tranzactions 
    WHERE tr_caid = @NEW_tr_caid AND tr_date <= @NEW_tr_date AND tr_id < @NEW_tr_id 
    ORDER BY tr_date DESC, tr_id DESC 
    LIMIT 1 
    INTO @balance;

SET @amount = @sign * @NEW_tr_amount;

SELECT @NEW_tr_id, @NEW_tr_date, @NEW_tr_caid, @NEW_tr_amount, @NEW_tr_clname, @amount, @balance;

UPDATE tranzactions SET tr_tmp_newbalance = tr_newbalance + @amount 
WHERE tr_date >= @NEW_tr_date AND tr_pivot = FALSE;

UPDATE tranzactions SET tr_tmp_newbalance = tr_amount - @amount 
WHERE tr_date > @NEW_tr_date AND tr_pivot = TRUE;

SELECT * from tranzactions WHERE tr_date >= @NEW_tr_date ORDER BY tr_date ASC, tr_id ASC;


-- This is a very usefull view
DROP VIEW IF EXISTS tranzactions_ordered;
CREATE VIEW tranzactions_ordered AS 
SELECT DATE_ADD(tr_date, INTERVAL tr_id SECOND) AS tr_dateid, tranzactions.*, cl_direction AS 'tr_sgn' 
FROM tranzactions INNER JOIN clusters ON cl_name = tr_clname 
ORDER BY tr_dateid;

SELECT * FROM tranzactions_ordered;

SELECT * FROM tranzactions_ordered
WHERE tr_dateid > "2015-01-23" AND tr_pivot
LIMIT 1;

-- Get last balance from a given time
SELECT * FROM tranzactions_ordered
    WHERE tr_caid = "pkez" AND tr_dateid < "2015-02-12 00:07:11"
    ORDER BY tr_dateid DESC
    LIMIT 4;


DROP PROCEDURE IF EXISTS proceed_tranzaction;
DELIMITER $$
CREATE PROCEDURE proceed_tranzaction (IN in_date DATE, IN in_id INTEGER, IN in_amount INTEGER, IN in_caid VARCHAR(8))
SQL SECURITY DEFINER
BEGIN
    DECLARE tr_dateid_actual DATETIME;
    DECLARE tr_dateid_until DATETIME;

    DECLARE tr_id_prev INTEGER;
    DECLARE tr_id_pivot INTEGER;
    DECLARE balance INTEGER;

    -- collect local variables

    SET tr_dateid_actual = DATE_ADD(in_date, INTERVAL in_id SECOND);

    SELECT tr_dateid, tr_id FROM tranzactions_ordered
        WHERE tr_dateid > tr_dateid_actual AND tr_pivot
        LIMIT 1
        INTO tr_dateid_until, tr_id_pivot;

    SELECT tr_newbalance, tr_id FROM tranzactions_ordered
        WHERE tr_caid = in_caid AND tr_dateid < tr_dateid_actual
        ORDER BY tr_dateid DESC LIMIT 1 
        INTO balance, tr_id_prev;

    -- SELECT tr_newbalance FROM tranzactions_ordered
    --     WHERE tr_caid = in_caid AND tr_dateid < tr_dateid_actual
    --     ORDER BY tr_dateid DESC LIMIT 1;

    -- diplay local variables

    -- SELECT in_date, in_id, in_amount, in_caid, tr_dateid_actual, tr_dateid_until, tr_id_pivot, balance;

    -- update what needed to be updated

    UPDATE tranzactions SET tr_tmp_newbalance = tr_newbalance + in_amount 
    WHERE 
        tr_caid = in_caid AND 
        DATE_ADD(tr_date, INTERVAL tr_id SECOND) BETWEEN tr_dateid_actual AND tr_dateid_until;

    UPDATE tranzactions SET tr_tmp_newbalance = tr_amount - in_amount WHERE tr_id = tr_id_pivot;

    -- display result set

    -- SELECT * FROM tranzactions, (SELECT "PREVIOUS" as status) as a 
    --     WHERE tr_id = tr_id_prev
    
    -- UNION -- UNION
    -- SELECT * FROM tranzactions, (SELECT "INSERTED" as status) as a 
    --     WHERE tr_id = in_id
    
    -- UNION -- UNION
    -- SELECT * FROM tranzactions, (SELECT "UPDATED" as status) as a
    --     WHERE tr_caid = in_caid AND (DATE_ADD(tr_date, INTERVAL tr_id SECOND) BETWEEN tr_dateid_actual AND tr_dateid_until)
    
    -- UNION -- UNION
    -- SELECT * FROM tranzactions, (SELECT "VALIDATED" as status) as a
    --     WHERE tr_id = tr_id_pivot;
END;
$$
DELIMITER ;

call proceed_tranzaction("2015-01-24", 671, 1234, "pkez");


-- triggerek
DROP TRIGGER IF EXISTS `tranzactions_after_insert_trigger`;

DELIMITER $$
CREATE DEFINER=`root`@`localhost` TRIGGER `tranzactions_after_insert_trigger` 
AFTER INSERT ON `tranzactions` FOR EACH ROW 
BEGIN
    -- CALL proceed_tranzaction(NEW.tr_date, NEW.tr_id, NEW.tr_amount, NEW.tr_caid);
    -- DECLARE sign INTEGER;
    -- DECLARE balance INTEGER;
    -- DECLARE amount INTEGER;

    -- SELECT cl_direction FROM clusters WHERE NEW.tr_clname = cl_name INTO sign;
    -- SELECT tr_newbalance FROM tranzactions WHERE tr_date <= NEW.tr_date AND tr_caid = NEW.tr_caid ORDER BY tr_date DESC, tr_id DESC LIMIT 1 INTO balance;

    -- SET amount = sign * NEW.tr_amount;
    -- SET NEW.tr_newbalance = balance + amount;

    -- UPDATE tranzactions SET tr_newbalance = tr_newbalance + amount WHERE tr_date > NEW.tr_date AND tr_pivot = FALSE;
    -- UPDATE tranzactions SET tr_amount = tr_amount - amount WHERE tr_date > NEW.tr_date AND tr_pivot = TRUE;
END; 
$$

INSERT INTO `javadrafts2`.`tranzactions` 
    (`tr_id`, `tr_date`, `tr_caid`, `tr_amount`, `tr_newbalance`, `tr_clname`, `tr_mkname`, `tr_remark`, `tr_remark_extra`, `tr_pivot`) 
VALUES 
    (NULL, '2015-01-23', 'pkez', '1234', NULL, 'Elektr_Cikk', 'Interspar', NULL, "EXTRA_BETET", '0');

