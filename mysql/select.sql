select tr_date, tr_amount, tr_caid, tr_clname, tr_mkname, tr_remark, tr_remark_extra 
from transactions 
where tr_clname like 'Elektr%';


select tr_date, tr_amount, tr_caid, tr_clname, tr_mkname, tr_remark, tr_remark_extra 
from transactions 
where tr_clname like 'Elektr%';

select * from clusters;

-- depth = 2
select c1.cl_name, c1.cl_parent, c2.cl_parent 
from clusters as c1 
LEFT OUTER JOIN clusters as c2 
ON c1.cl_parent=c2.cl_name;

-- depth = 4
select n1, n2, n3, n4, c4.cl_parent as n5
from
(
    select n1, n2, n3, c3.cl_parent as n4
    from
    (
        select c1.cl_name as n1, c1.cl_parent as n2, c2.cl_parent as n3
        from clusters as c1 
        LEFT OUTER JOIN clusters as c2 
        ON c1.cl_parent=c2.cl_name
    ) as c12
    LEFT OUTER JOIN clusters as c3
    ON c12.n3=c3.cl_name
) as c123
LEFT OUTER JOIN clusters as c4
ON c123.n4=c4.cl_name;

-- depth = 4 (alternatively, a simpler approach)
select c1.cl_name, c2.cl_name, c3.cl_name, c4.cl_name, c4.cl_parent 
from clusters as c1 
LEFT OUTER JOIN clusters as c2 
ON c1.cl_parent=c2.cl_name
LEFT OUTER JOIN clusters as c3
ON c2.cl_parent=c3.cl_name
LEFT OUTER JOIN clusters as c4
ON c3.cl_parent=c4.cl_name;

DROP VIEW IF EXISTS trflaged;
CREATE VIEW trflaged AS 
select tr.*, 
    (tr_clname like 'Athelyezes_%' and not isnull(tr_clname)) as 'tr_A' 
from transactions as tr
order by tr_date;
SELECT * FROM trflaged;

-- havi osszkoltseg, osszbevetel
drop view if exists monthly_sum;
create view monthly_sum as
select tr_date, 
    max(get_balance(tr_date, 'potp')) as 'potp', 
    max(get_balance(tr_date, 'pkez')) as 'pkez', 
    year(tr_date) as yyyy, month(tr_date)+1 as m, 
    sum(IF(tr_amount < 0, tr_amount, 0)) as kiadas,
    sum(IF(tr_amount > 0, tr_amount, 0)) as bevetel
from trflaged as tr
where not tr_A
group by yyyy, m;

set @csum := 0;
select tr_date, 
    concat(yyyy, ', ', LPAD(concat(FORMAT(m,0),'. het'), 7, ' ')) as 'het',
    LPAD(FORMAT(potp, 0, 'hu_HU'), 10, ' ') as "potp (max)",
    LPAD(FORMAT(pkez, 0, 'hu_HU'), 10, ' ') as "pkez (max)",
    LPAD(FORMAT(kiadas, 0, 'hu_HU'), 10, ' ') as kiadas,
    LPAD(FORMAT(bevetel, 0, 'hu_HU'), 10, ' ') as bevetel,
    LPAD(FORMAT(bevetel+kiadas, 0, 'hu_HU'), 10, ' ') as diff, 
    LPAD(FORMAT((@csum := @csum + bevetel+kiadas), 0, 'hu_HU'), 10, ' ') as cumsum
from monthly_sum as tr;

-- heti osszkoltseg, osszbevetel
drop view if exists weekly_sum;
create view weekly_sum as
select tr_date, 
    max(get_balance(tr_date, 'potp')) as 'potp', 
    max(get_balance(tr_date, 'pkez')) as 'pkez', 
    year(tr_date) as yyyy, week(tr_date)+1 as w, 
    sum(IF(tr_amount < 0, tr_amount, 0)) as kiadas,
    sum(IF(tr_amount > 0, tr_amount, 0)) as bevetel
from trflaged as tr
where not tr_A
group by yyyy, w;

set @csum := 0;
select tr_date, 
    concat(yyyy, ', ', LPAD(concat(FORMAT(w,0),'. het'), 7, ' ')) as 'het',
    LPAD(FORMAT(potp, 0, 'hu_HU'), 10, ' ') as potp,
    LPAD(FORMAT(pkez, 0, 'hu_HU'), 10, ' ') as pkez,
    LPAD(FORMAT(kiadas, 0, 'hu_HU'), 10, ' ') as kiadas,
    LPAD(FORMAT(bevetel, 0, 'hu_HU'), 10, ' ') as bevetel,
    LPAD(FORMAT(bevetel+kiadas, 0, 'hu_HU'), 10, ' ') as diff, 
    LPAD(FORMAT((@csum := @csum + bevetel+kiadas), 0, 'hu_HU'), 10, ' ') as cumsum
from weekly_sum as tr;





DROP FUNCTION IF EXISTS get_balance;
DELIMITER $$
CREATE FUNCTION get_balance(in_date DATE, in_caid VARCHAR(8))
    RETURNS INT READS SQL DATA
BEGIN
    DECLARE balance INT;
    select tr_newbalance from trflaged where tr_caid = in_caid and in_date <= tr_date limit 1 into balance;
    RETURN balance;
END;
$$
DELIMITER ;
select get_balance('2015-03-20','potp');

select * from trflaged where tr_caid = 'potp' and '2015-03-20' <= tr_date limit 1;

-- azt akartam megtudni, hogy mire ment el a penz amiota kaptunk ocsitol [NAGY KOLTSEGEK]
SELECT * FROM trflaged where tr_date > date('2015-05-03') and tr_amount < -5000 and (tr_caid = 'pkez') and not tr_A;

-- mennyit koltottem (pkez) amiota jott a penz ocsitol, kumulative, kis es nagykoltseg egyarant
set @csum := 0; SELECT trflaged.*, (@csum := @csum + tr_amount) as cumsum FROM trflaged where tr_date > date('2015-05-03') and tr_amount < 0 and (tr_caid = 'pkez') and not tr_A;

-- mennyit koltottunk (pkez + dkez) -------=--------
set @csum := 0; SELECT trflaged.*, (@csum := @csum + tr_amount) as cumsum FROM trflaged where tr_date > date('2015-05-03') and tr_amount < 0 and (tr_caid = 'pkez' or tr_caid = 'dkez') and not tr_A;

-- [REALIS - MINDEN - order by tr_amount DESC] mint az elozo, csak dkez folosleges szamolasa nelkul
set @csum := 0; SELECT trflaged.*, (@csum := @csum + tr_amount) as cumsum FROM trflaged where tr_date > date('2015-05-03') and tr_amount < -000 and (tr_caid = 'pkez' or tr_caid = 'dkez') and not tr_A and not tr_amount = -23462 order by tr_amount DESC;

-- [REALIS - szelektalva: Rezsi lakas gyogyszer]
set @csum := 0; SELECT trflaged.*, (@csum := @csum + tr_amount) as cumsum FROM trflaged where tr_date > date('2015-05-03') and tr_amount < -000 and (tr_caid = 'pkez' or tr_caid = 'dkez') and not tr_A and not tr_amount = -23462 and \
((not tr_clname like 'Rezsi_%' and not tr_clname like 'Gyogyszer%' and not tr_clname like 'Lakas_%') or isnull(tr_clname)) order by tr_amount DESC;

-- [REALIS - extrak]
set @csum := 0; SELECT trflaged.*, (@csum := @csum + tr_amount) as cumsum FROM trflaged where tr_date > date('2015-05-03') and tr_amount < -000 and (tr_caid = 'pkez' or tr_caid = 'dkez') and not tr_A and not tr_amount = -23462 and \
(tr_clname like 'Rezsi_%' or tr_clname like 'Gyogyszer%' or tr_clname like 'Lakas_%') order by tr_amount DESC;

-- [REALIS - csak rezsi + lkbbrn]
set @csum := 0; SELECT trflaged.*, (@csum := @csum + tr_amount) as cumsum FROM trflaged where tr_date > date('2014-08-10') and tr_amount < -000 and (tr_caid = 'pkez' or tr_caid = 'dkez') and not tr_A and not tr_amount = -23462 and \
(tr_clname like 'Rezsi_%' or tr_clname like 'Lakas_%') order by tr_amount DESC;

-- [REALIS - csak rezsi + lkbbrn]
set @csum := 0; SELECT trflaged.*, (@csum := @csum + tr_amount) as cumsum FROM trflaged where tr_date > date('2014-08-10') and tr_amount < -000 and (tr_caid = 'pkez' or tr_caid = 'dkez') and not tr_A and not tr_amount = -23462 and \
(tr_clname like 'Rezsi_%') order by tr_amount DESC;

-- [REALIS - minden]
set @csum := 0; SELECT trflaged.*, (@csum := @csum + tr_amount) as cumsum FROM trflaged where tr_date > date('2014-08-10') and tr_amount < -000 and not tr_A and not tr_amount = -23462 order by tr_amount DESC;

-- [REALIS - minden bevetel]
set @csum := 0; SELECT trflaged.*, (@csum := @csum + tr_amount) as cumsum FROM trflaged where tr_date > date('2014-08-10') and tr_amount > 0 and not tr_A and not tr_amount = -23462 order by tr_amount ASC;

-- [fizetes]
set @csum := 0; SELECT trflaged.*, (@csum := @csum + tr_amount) as cumsum FROM trflaged where tr_date > date('2014-08-10') and tr_amount > 0 and not tr_A and (tr_clname = 'Fizetes' or tr_clname = 'Osztondij') order by tr_amount ASC;

-- [otthonrol]
set @csum := 0; SELECT trflaged.*, (@csum := @csum + tr_amount) as cumsum FROM trflaged where tr_date > date('2014-08-10') and tr_amount > 0 and not tr_A and tr_clname like '%Otthon%' order by tr_amount ASC;
