
CREATE DATABASE IF NOT EXISTS drafts;

-- CREATE USER `guest`@`localhost` IDENTIFIED BY 'guestpass';
GRANT ALL PRIVILEGES ON `drafts`.* to 'guest'@'localhost' identified by 'guestpass' WITH GRANT OPTION;

GRANT ALL PRIVILEGES ON `koltsegvetes`.* to 'java'@'localhost' identified by 'javapass' WITH GRANT OPTION;


-- CREATE USER `guest`@`localhost` IDENTIFIED BY 'guestpass';
GRANT ALL PRIVILEGES ON `javadrafts2`.* to 'java'@'localhost' identified by 'javapass' WITH GRANT OPTION;

GRANT ALL PRIVILEGES ON `javadrafts_uj`.* to 'java'@'localhost' identified by 'javapass' WITH GRANT OPTION;

DROP DATABASE javadrafts2;
CREATE DATABASE IF NOT EXISTS javadrafts2;
USE javadrafts2;

DROP FUNCTION IF EXISTS last_insert_rowid;
DELIMITER $$
CREATE FUNCTION last_insert_rowid()
    RETURNS INT
BEGIN
    DECLARE id INT;
    SELECT last_insert_id() INTO id;
    RETURN id;
END;
$$
DELIMITER ;

