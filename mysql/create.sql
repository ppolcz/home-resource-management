
DROP DATABASE IF EXISTS jee_budget;

CREATE DATABASE IF NOT EXISTS jee_budget;
USE jee_budget;

CREATE TABLE t_cluster (
    uid INT NOT NULL AUTO_INCREMENT,
    --
    name VARCHAR(45),
    sgn INT NOT NULL,
    parent INT,
    INDEX parent_index (parent),
    PRIMARY KEY(uid)
) ENGINE = InnoDB, DEFAULT CHARACTER SET = utf8;

ALTER TABLE t_cluster ADD CONSTRAINT cns_clusters_parent
FOREIGN KEY (parent)
REFERENCES t_cluster(uid)
ON DELETE RESTRICT ON UPDATE RESTRICT;

CREATE TABLE t_charge_accounts (
    uid INT NOT NULL AUTO_INCREMENT,
    --
    name VARCHAR(8),
    description VARCHAR(100),
    PRIMARY KEY(uid)
) ENGINE = InnoDB, DEFAULT CHARACTER SET = utf8;

CREATE TABLE t_markets (
    uid INT NOT NULL AUTO_INCREMENT,
    --
    name VARCHAR(45),
    description VARCHAR(256),
    PRIMARY KEY(uid)
) ENGINE = InnoDB, DEFAULT CHARACTER SET = utf8;

CREATE TABLE t_transactions ( 
    uid INT NOT NULL AUTO_INCREMENT,
    --
    date DATE NOT NULL, 
    balance INT NOT NULL DEFAULT 0,
    amount INT NOT NULL DEFAULT 0,
    remark VARCHAR(100) DEFAULT NULL,
    PRIMARY KEY (uid), 
    --
    ca INT NOT NULL,
    cluster INT NOT NULL,
    market INT,
    INDEX caid_index (ca), 
    INDEX cluster_index (cluster),
    INDEX market_index (market)
) ENGINE = InnoDB, DEFAULT CHARACTER SET = utf8;

ALTER TABLE t_transactions ADD CONSTRAINT cns_transactions_caid 
FOREIGN KEY (ca) 
REFERENCES t_charge_accounts(uid) 
ON DELETE RESTRICT ON UPDATE RESTRICT; 

ALTER TABLE t_transactions ADD CONSTRAINT cns_transactions_cluster 
FOREIGN KEY (cluster) 
REFERENCES t_cluster(uid) 
ON DELETE RESTRICT ON UPDATE RESTRICT;

ALTER TABLE t_transactions ADD CONSTRAINT cns_transactions_market 
FOREIGN KEY (market) 
REFERENCES t_markets(uid) 
ON DELETE RESTRICT ON UPDATE RESTRICT;

DROP TABLE IF EXISTS t_product_infos;
CREATE TABLE t_product_infos (
    uid INT NOT NULL AUTO_INCREMENT,
    --
    amount INT NOT NULL,
    amount_orig INT DEFAULT NULL,
    currency VARCHAR(8) DEFAULT 'Ft',
    description VARCHAR(300) NOT NULL,
    PRIMARY KEY (uid),
    --
    trid INT DEFAULT NULL,
    market INT NOT NULL,
    cluster INT NOT NULL,
    INDEX trid_index (trid),
    INDEX market_index (market),
    INDEX cluster_index (cluster)
) ENGINE = InnoDB, DEFAULT CHARACTER SET = utf8;

ALTER TABLE t_product_infos ADD CONSTRAINT cns_product_info_trid 
FOREIGN KEY (trid) 
REFERENCES t_transactions(uid) 
ON DELETE RESTRICT ON UPDATE RESTRICT;

ALTER TABLE t_product_infos ADD CONSTRAINT cns_product_info_cluster 
FOREIGN KEY (cluster) 
REFERENCES t_cluster(uid) 
ON DELETE RESTRICT ON UPDATE RESTRICT;

ALTER TABLE t_product_infos ADD CONSTRAINT cns_product_info_market 
FOREIGN KEY (market) 
REFERENCES t_markets(uid) 
ON DELETE RESTRICT ON UPDATE RESTRICT;

INSERT INTO t_cluster (name, sgn) VALUES 
('Napi_Szukseglet', -1),
('Rezsi_Bkv', -1),
('Rezsi_Upc', -1),
('Rezsi_Viz', -1),
('Rezsi_Elmu', -1),
('Rezsi_Fotav', -1),
('Rezsi_Fogaz', -1),
('Rezsi_Kozosk', -1),
('Kaucio', -1),
('Kaucio_Vissza', +1),
('Elelem', -1),
('Ruha_Dori', -1),
('Ruha_Peti', -1),
('Alberlet', -1),
('Osztondij', +1),
('Fizetes', +1),
('Otthon', +1),
('Otthon_Jozsa', +1),
('Egyeb_Kiadas', -1),
('Egyeb_Bevetel', +1),
('Athelyezes_Innen', -1),
('Athelyezes_Ide', +1),
('Lux_Egyeb', -1),
('Lux_Mozi', -1),
('Korrigalas', 1);

INSERT INTO t_charge_accounts (name, description) VALUES 
('pkez', 'Peti Kezpenz'),
('dkez', 'Dori Kezpenz'),
('potp', 'Peti OTP Bank'),
('dotp', 'Dori OTP Bank');

INSERT INTO t_markets (name) VALUES 
('Not_applicable'),
('Interspar'),
('Spar'),
('Decathlon'),
('Auchan'),
('IKEA');

INSERT INTO drafts.t_transactions (id, date, ca, balance, amount, cluster, market, remark) VALUES 
(NULL, '2015-03-24', 'pkez', '0', '1200', 'Napi_Szukseglet', 'Interspar', 'kaja'), 
(NULL, '2015-03-18', 'pkez', '0', '1400', 'Rezsi_Elmu', NULL, NULL),
(NULL, '2015-02-05', 'pkez', '0', '1231', 'Napi_Szukseglet', NULL, NULL),
(NULL, '2015-03-26', 'pkez', '0', '1312', 'Napi_Szukseglet', NULL, NULL),
(NULL, '2015-02-22', 'pkez', '0', '4100', 'Napi_Szukseglet', NULL, NULL),
(NULL, '2015-03-01', 'pkez', '0', '4030', 'Napi_Szukseglet', NULL, NULL),
(NULL, '2015-03-03', 'pkez', '0', '3100', 'Napi_Szukseglet', NULL, NULL),
(NULL, '2015-05-24', 'pkez', '0', '1220', 'Napi_Szukseglet', 'Interspar', 'kaja'), 
(NULL, '2015-05-18', 'pkez', '0', '1420', 'Rezsi_Elmu', NULL, NULL),
(NULL, '2015-05-05', 'pkez', '0', '1521', 'Napi_Szukseglet', NULL, NULL),
(NULL, '2015-05-26', 'pkez', '0', '1512', 'Napi_Szukseglet', NULL, NULL),
(NULL, '2015-05-22', 'pkez', '0', '4500', 'Napi_Szukseglet', NULL, NULL),
(NULL, '2015-05-01', 'pkez', '0', '6030', 'Napi_Szukseglet', NULL, NULL),
(NULL, '2015-05-03', 'pkez', '0', '6100', 'Napi_Szukseglet', NULL, NULL),
(NULL, '2015-05-21', 'pkez', '0', '6020', 'Napi_Szukseglet', NULL, NULL);

SELECT * FROM t_transactions WHERE ca like 'pkez' ORDER BY date;



------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------

-- ez jol mukodik
DROP PROCEDURE IF EXISTS get_last_transaction;
DELIMITER $$
CREATE PROCEDURE get_last_transaction(IN in_date DATE, IN in_caName VARCHAR(8))
SQL SECURITY DEFINER
BEGIN
    SELECT * FROM t_transactions AS tr, t_charge_accounts AS ca WHERE tr.ca = ca.uid AND date < in_date AND ca.name = in_caName ORDER BY tr.date DESC LIMIT 1;
END;
$$
DELIMITER ;
-- test:
SELECT * FROM t_transactions AS tr, t_charge_accounts AS ca WHERE tr.ca = ca.uid AND date < "2015-12-24" AND ca.name = "pkez" ORDER BY tr.date DESC LIMIT 1;
CALL get_last_transaction("2015-12-24", "pkez");




-- Get all
SELECT * FROM t_transactions
ORDER BY date, pivot, uid;

-- Get first pivot from a given time
SELECT * FROM t_transactions
WHERE ca = "1" AND pivot AND date < "2015-12-09"
ORDER BY date DESC, pivot, uid 
LIMIT 1;

-- Get all dirty rows
SELECT * FROM t_transactions
WHERE (ca = "1" OR catransfer = "1") AND date > "2015-12-08"
ORDER BY date, pivot, uid;



##################################################################################################
## TRANZACTIONS_ORDERED [BEGIN] ##################################################################
##################################################################################################

DROP FUNCTION IF EXISTS to_dateid;
DELIMITER $$
CREATE FUNCTION to_dateid(in_date DATE, in_id INT) 
    RETURNS DATETIME
BEGIN
    RETURN DATE_ADD(in_date, INTERVAL in_id SECOND);
END $$
DELIMITER ;

-- This is a very usefull view
DROP VIEW IF EXISTS tranzactions_ordered;
CREATE VIEW tranzactions_ordered AS 
SELECT to_dateid(tr_date, tr_id) AS tr_dateid, tranzactions.*, cl_direction AS 'tr_sgn' 
FROM tranzactions INNER JOIN clusters ON cl_name = tr_clname 
ORDER BY tr_dateid;

-- This is a very usefull view
DROP VIEW IF EXISTS tranzactions_ordered_desc;
CREATE VIEW tranzactions_ordered_desc AS 
SELECT to_dateid(tr_date, tr_id) AS tr_dateid, tranzactions.*, cl_direction AS 'tr_sgn' 
FROM tranzactions INNER JOIN clusters ON cl_name = tr_clname 
ORDER BY tr_dateid DESC;

SELECT * FROM tranzactions_ordered;
SELECT * FROM tranzactions_ordered_desc LIMIT 1;


-- Get last balance from a given time
SELECT * FROM tranzactions_ordered
WHERE tr_caid = "pkez" AND tr_dateid < "2015-02-12 00:07:11"
ORDER BY tr_dateid DESC
LIMIT 4;

##################################################################################################
## UPDATE INTERVALS [BEGIN] ######################################################################
##################################################################################################

# STATUS:
-- 0. update registered, ready to preceed
-- 1. update done, but not tested
-- 2. update done and tested, ready to forget about it
DROP TABLE IF EXISTS update_intervals;
CREATE TABLE IF NOT EXISTS update_intervals (
    ui_actual DATETIME,
    ui_pivot DATETIME,
    ui_caid VARCHAR(8),
    ui_status INTEGER DEFAULT 0,
    PRIMARY KEY(ui_pivot, ui_caid)
) default character set = utf8;

DROP PROCEDURE IF EXISTS append_update_interval;
DELIMITER $$
CREATE PROCEDURE append_update_interval(IN in_actual DATETIME, IN in_pivot DATETIME, IN in_caid VARCHAR(8))
BEGIN
    DECLARE actual DATETIME;
    DECLARE merge DATETIME;

    SELECT ui_actual FROM update_intervals 
    WHERE ui_pivot = in_pivot AND ui_caid = in_caid
    INTO actual;

    SELECT ui_actual FROM update_intervals
    WHERE ui_pivot = in_actual AND ui_caid = in_caid
    INTO merge;

    IF merge IS NOT NULL THEN
        UPDATE update_intervals SET ui_pivot = in_pivot, ui_status = 0
        WHERE ui_pivot = in_actual AND ui_caid = in_caid;
    ELSEIF actual IS NULL THEN
        INSERT INTO update_intervals (ui_actual, ui_pivot, ui_caid) VALUES (in_actual, in_pivot, in_caid);
    ELSEIF in_actual < actual THEN
        UPDATE update_intervals SET ui_actual = in_actual, ui_status = 0
        WHERE ui_pivot = in_pivot AND ui_caid = in_caid;
    END IF;
END $$
DELIMITER ;

-- INSERT INTO update_intervals VALUES
-- ("2015-11-01", "2015-12-02", "pkez"),
-- ("2014-11-02", "2014-12-02", "pkez"),
-- ("2013-09-03", "2013-09-07", "pkez"),
-- ("2015-02-01", "2015-02-19", "pkez");

-- CALL append_update_interval("2015-11-02", "2015-12-02", "pkez");
-- SELECT * FROM update_intervals;
-- CALL append_update_interval("2015-10-02", "2015-12-02", "pkez");
-- SELECT * FROM update_intervals;

##################################################################################################
## SHOW RAWS NEEDED TO UPDATE [BEGIN] ############################################################
##################################################################################################

-- raw select
SELECT tr_dateid, tr_id, tr_caid, tr_amount, tr_newbalance, tr_clname, tr_mkname, tr_remark, tr_remark_extra, 
    IF(tr_pivot, "PIVOT","") as pivot, 
    IF(MAX(status) = 1, "UPDATE", "") as status,
    CASE ui_status 
        WHEN 0 THEN "UPDATE..."
        WHEN 1 THEN "UPDATED"
        WHEN 2 THEN "TESTED"
        ELSE ""
    END as ui_status
FROM
(
    SELECT tranzactions_ordered.*, 1 as status, ui_status
    FROM tranzactions_ordered, update_intervals
    WHERE tr_caid = ui_caid AND (tr_dateid BETWEEN ui_actual AND ui_pivot)
UNION
    SELECT tranzactions_ordered.*, 0 as status, -1 as ui_status FROM tranzactions_ordered
) as dummy
GROUP BY tr_dateid
ORDER BY tr_dateid;

DROP VIEW IF EXISTS tranzactions_status_update;
CREATE VIEW tranzactions_status_update AS
    SELECT tranzactions_ordered.*, 1 as status, ui_status
    FROM tranzactions_ordered, update_intervals
    WHERE tr_caid = ui_caid AND (tr_dateid BETWEEN ui_actual AND ui_pivot)
UNION
    SELECT tranzactions_ordered.*, 0 as status, -1 as ui_status FROM tranzactions_ordered;

DROP VIEW IF EXISTS tranzactions_status;
CREATE VIEW tranzactions_status AS
SELECT tr_dateid, tr_id, tr_caid, tr_amount, tr_newbalance, tr_clname, tr_mkname, tr_remark, tr_remark_extra, 
    IF(tr_pivot, "PIVOT","") as pivot, 
    IF(MAX(status) = 1, "UPDATE", "") as status,
    CASE ui_status 
        WHEN 0 THEN "UPDATE..."
        WHEN 1 THEN "UPDATED"
        WHEN 2 THEN "TESTED"
        ELSE ""
    END as ui_status
FROM tranzactions_status_update
GROUP BY tr_dateid
ORDER BY tr_dateid;

DROP VIEW IF EXISTS tranzactions_status_desc;
CREATE VIEW tranzactions_status_desc AS
SELECT tr_dateid, tr_id, tr_caid, tr_amount, tr_newbalance, tr_clname, tr_mkname, tr_remark, tr_remark_extra, tr_pivot,
    MAX(status) as status, ui_status 
FROM tranzactions_status_update
GROUP BY tr_dateid
ORDER BY tr_dateid DESC;

SELECT * FROM tranzactions_status_desc LIMIT 50;
SELECT * FROM tranzactions_status;
SELECT * FROM update_intervals;

##################################################################################################
## SHOW RAWS NEEDED TO UPDATE [END] ##############################################################
##################################################################################################


DROP PROCEDURE IF EXISTS proceed_tranzaction;
DELIMITER $$
CREATE PROCEDURE proceed_tranzaction (IN in_date DATE, IN in_id INTEGER, IN in_amount INTEGER, IN in_caid VARCHAR(8))
SQL SECURITY DEFINER
BEGIN
    DECLARE tr_dateid_actual DATETIME;
    DECLARE tr_dateid_until DATETIME;

    DECLARE tr_id_prev INTEGER;
    DECLARE tr_id_pivot INTEGER;
    DECLARE balance INTEGER;

    -- collect local variables

    SET tr_dateid_actual = to_dateid(in_date, in_id);

    SELECT tr_dateid, tr_id FROM tranzactions_ordered
        WHERE tr_dateid > tr_dateid_actual AND tr_pivot
        LIMIT 1
        INTO tr_dateid_until, tr_id_pivot;

    SELECT tr_newbalance, tr_id FROM tranzactions_ordered
        WHERE tr_caid = in_caid AND tr_dateid < tr_dateid_actual
        ORDER BY tr_dateid DESC LIMIT 1 
        INTO balance, tr_id_prev;

    -- SELECT tr_newbalance FROM tranzactions_ordered
    --     WHERE tr_caid = in_caid AND tr_dateid < tr_dateid_actual
    --     ORDER BY tr_dateid DESC LIMIT 1;

    -- diplay local variables

    -- SELECT in_date, in_id, in_amount, in_caid, tr_dateid_actual, tr_dateid_until, tr_id_pivot, balance;

    -- update what needed to be updated

    UPDATE tranzactions SET tr_tmp_newbalance = tr_newbalance + in_amount 
    WHERE 
        tr_caid = in_caid AND 
        to_dateid(tr_date, tr_id) BETWEEN tr_dateid_actual AND tr_dateid_until;

    UPDATE tranzactions SET tr_tmp_newbalance = tr_amount - in_amount WHERE tr_id = tr_id_pivot;

    -- display result set

    -- SELECT * FROM tranzactions, (SELECT "PREVIOUS" as status) as a 
    --     WHERE tr_id = tr_id_prev
    
    -- UNION -- UNION
    -- SELECT * FROM tranzactions, (SELECT "INSERTED" as status) as a 
    --     WHERE tr_id = in_id
    
    -- UNION -- UNION
    -- SELECT * FROM tranzactions, (SELECT "UPDATED" as status) as a
    --     WHERE tr_caid = in_caid AND (to_dateid(tr_date, tr_id) BETWEEN tr_dateid_actual AND tr_dateid_until)
    
    -- UNION -- UNION
    -- SELECT * FROM tranzactions, (SELECT "VALIDATED" as status) as a
    --     WHERE tr_id = tr_id_pivot;
END;
$$
DELIMITER ;

call proceed_tranzaction("2015-01-24", 671, 1234, "pkez");


##################################################################################################
## INSERT_TRIGGER [BEGIN] ########################################################################
##################################################################################################

INSERT INTO clusters (cl_name, cl_direction, cl_description) VALUES
("Szamolas", 1, "tr_newbalance: tenyleges penz ami van, tr_amount: hiany")

DROP TRIGGER IF EXISTS `tranzactions_before_insert_trigger`;
DELIMITER $$
CREATE DEFINER=`root`@`localhost` TRIGGER `tranzactions_before_insert_trigger` 
    BEFORE INSERT ON `tranzactions` FOR EACH ROW 
BEGIN
    DECLARE sign INTEGER;
    DECLARE balance INTEGER;
    DECLARE in_actual DATETIME;
    DECLARE in_pivot DATETIME;

    SELECT cl_direction FROM clusters WHERE NEW.tr_clname = cl_name INTO sign;
    SET in_actual = DATE_ADD(DATE_ADD(NEW.tr_date, INTERVAL 1 DAY), INTERVAL -1 SECOND);

    -- Get last balance before actual
    SELECT tr_newbalance FROM tranzactions_ordered
    WHERE tr_caid = "pkez" AND tr_dateid < in_actual
    ORDER BY tr_dateid DESC
    LIMIT 1
    INTO balance;

    IF NEW.tr_pivot THEN
        SET NEW.tr_clname = "Szamolas";
        SET NEW.tr_amount = NEW.tr_newbalance - balance;
    ELSE
        SET NEW.tr_newbalance = balance + sign * NEW.tr_amount;
    END IF;
END $$
DELIMITER ;

-- triggerek
DROP TRIGGER IF EXISTS `tranzactions_after_insert_trigger`;
DELIMITER $$
CREATE DEFINER=`root`@`localhost` TRIGGER `tranzactions_after_insert_trigger` 
AFTER INSERT ON `tranzactions` FOR EACH ROW 
BEGIN
    DECLARE sign INTEGER;
    DECLARE in_actual DATETIME;
    DECLARE in_pivot DATETIME;
    DECLARE a_is_pivot BOOLEAN;
    DECLARE a_is_dirty BOOLEAN;
    DECLARE a_dateid DATETIME;

    SELECT cl_direction FROM clusters WHERE NEW.tr_clname = cl_name INTO sign;
    SET in_actual = to_dateid(NEW.tr_date, NEW.tr_id);

    -- Get last tranzaction before actually inserted (a)
    SELECT tr_pivot, tr_dateid, status FROM tranzactions_status_desc
    WHERE tr_caid = NEW.tr_caid AND tr_dateid < in_actual AND status = 1
    LIMIT 1
    INTO a_is_pivot, a_dateid, a_is_dirty;

    -- If a is not pivot && a is dirty
    IF NOT a_is_pivot AND a_is_dirty THEN
        SET in_actual = a_dateid;
    END IF;

    -- Get next pivot if exists (p)
    SELECT tr_dateid FROM tranzactions_ordered
    WHERE tr_caid = NEW.tr_caid AND tr_dateid > in_actual AND tr_pivot
    LIMIT 1
    INTO in_pivot;

    -- If no pivots after the newly inserted (l)
    IF in_pivot IS NULL THEN
        SELECT tr_dateid FROM tranzactions_ordered_desc LIMIT 1 INTO in_pivot;
    END IF;

    IF NEW.tr_caid IS NULL THEN
        SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'You must specify the charge account';
    END IF;

    CALL append_update_interval(in_actual, in_pivot, NEW.tr_caid);
END $$
DELIMITER ;

TRUNCATE TABLE update_intervals;

INSERT INTO `javadrafts2`.`tranzactions` 
    (`tr_id`, `tr_date`, `tr_caid`, `tr_amount`, `tr_newbalance`, `tr_clname`, `tr_mkname`, `tr_remark`, `tr_remark_extra`, `tr_pivot`) 
VALUES 
    (NULL, '2015-02-09', 'pkez', '1234', NULL, 'Elektr_Cikk', 'Interspar', NULL, "EXTRA_BETET", '0');

INSERT INTO `javadrafts2`.`tranzactions` 
    (`tr_id`, `tr_date`, `tr_caid`, `tr_amount`, `tr_newbalance`, `tr_clname`, `tr_mkname`, `tr_remark`, `tr_remark_extra`, `tr_pivot`) 
VALUES 
    (NULL, '2015-01-09', 'pkez', '1234', NULL, 'Elektr_Cikk', 'Interspar', NULL, "EXTRA_BETET", '0');

INSERT INTO `javadrafts2`.`tranzactions` 
    (`tr_id`, `tr_date`, `tr_caid`, `tr_amount`, `tr_newbalance`, `tr_clname`, `tr_mkname`, `tr_remark`, `tr_remark_extra`, `tr_pivot`) 
VALUES 
    (NULL, '2014-12-09', 'pkez', '1234', NULL, 'Elektr_Cikk', 'Interspar', NULL, "EXTRA_BETET", '0');

TRUNCATE TABLE update_intervals;
SELECT * FROM tranzactions_status;
SELECT * FROM update_intervals;


##################################################################################################
## UPDATE_ [BEGIN] ########################################################################
##################################################################################################

DROP PROCEDURE IF EXISTS do_update_on;
DELIMITER $$
CREATE PROCEDURE do_update_on (in_first DATETIME, in_last DATETIME, in_caid VARCHAR(8))
SQL SECURITY DEFINER
BEGIN
    -- DECLARE tr_dateid_actual DATETIME;
    -- DECLARE tr_dateid_until DATETIME;

    -- DECLARE tr_id_prev INTEGER;
    DECLARE pivot_reached BOOLEAN DEFAULT FALSE;
    DECLARE prev_id, prev_amount, prev_balance, prev_sgn INTEGER;
    DECLARE id, amount, balance, sgn, pivot INTEGER;

    DECLARE done INT DEFAULT 0;
    DECLARE cur CURSOR FOR 
        SELECT tr_id, tr_amount, tr_newbalance, tr_sgn, tr_pivot from tranzactions_ordered 
        WHERE tr_caid = in_caid AND (tr_dateid BETWEEN in_first AND in_last); 
    DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = 1;

    -- SELECT * from tranzactions_ordered 
    -- WHERE tr_caid = in_caid AND (tr_dateid BETWEEN in_first AND in_last); 

    OPEN cur;

    -- read first row, which should NOT be updated
    FETCH cur INTO prev_id, prev_amount, prev_balance, prev_sgn, pivot;

    loop1: LOOP
        FETCH cur INTO id, amount, balance, sgn, pivot;
    
        IF done THEN
            LEAVE loop1;
        END IF;

        IF pivot_reached THEN
            SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'One update interval should contain only one pivot row at the end of the interval';
        END IF;

        IF pivot THEN
            UPDATE tranzactions SET tr_amount = tr_newbalance - prev_balance WHERE tr_id = id;
            SET pivot_reached = TRUE;
        ELSE 
            SET prev_balance = prev_balance + sgn * amount;
            UPDATE tranzactions SET tr_newbalance = prev_balance WHERE tr_id = id;
        END IF;

        SET prev_id = id, prev_amount = amount, prev_sgn = sgn;
    END LOOP;
END;
$$
DELIMITER ;

DROP PROCEDURE IF EXISTS do_update;
DELIMITER $$
CREATE PROCEDURE do_update()
BEGIN
    DECLARE first, last DATETIME;
    DECLARE caid VARCHAR(8);

    DECLARE done INT DEFAULT 0;
    DECLARE cur CURSOR FOR SELECT ui_actual, ui_pivot, ui_caid from update_intervals where ui_status = 0; 
    DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = 1;

    SELECT * from update_intervals;
    OPEN cur;

    read_loop: LOOP
        FETCH cur INTO first, last, caid;
        
        IF done THEN
            LEAVE read_loop;
        END IF;

        SELECT first, last, caid;
        CALL do_update_on(first, last, caid);

        UPDATE update_intervals SET ui_status = 1 WHERE ui_caid = caid AND ui_pivot = last;
    END LOOP;
END $$
DELIMITER ;


DROP PROCEDURE IF EXISTS test_update;
DELIMITER $$
CREATE PROCEDURE test_update()
BEGIN
    DECLARE first, last DATETIME;
    DECLARE caid VARCHAR(8);
    DECLARE valid BOOLEAN;

    DECLARE done INT DEFAULT 0;
    DECLARE cur CURSOR FOR 
        SELECT ui_caid FROM update_intervals 
        WHERE ui_status = 1 GROUP BY ui_caid; 
    DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = 1;

    OPEN cur;
    read_loop: LOOP
        FETCH cur INTO caid;
        
        IF done THEN
            LEAVE read_loop;
        END IF;

        SELECT MIN(ui_actual) FROM update_intervals WHERE ui_caid = caid INTO first;
        SELECT MAX(ui_pivot) FROM update_intervals WHERE ui_caid = caid INTO last;

        SELECT first, last, caid;
        CALL test_validity(first, last, caid, valid);

        SELECT valid;

        IF valid THEN
            UPDATE update_intervals SET ui_status = 2 WHERE ui_caid = caid;
        END IF;
    END LOOP;
END $$
DELIMITER ;

CALL do_update();
SELECT * FROM tranzactions_status;
SELECT * FROM update_intervals;


DROP PROCEDURE IF EXISTS test_validity;
DELIMITER $$
CREATE PROCEDURE test_validity(first DATETIME, last DATETIME, caid VARCHAR(8), OUT is_valid BOOLEAN)
BEGIN
    DROP TEMPORARY TABLE IF EXISTS tmpall;
    DROP TEMPORARY TABLE IF EXISTS tmpl;
    DROP TEMPORARY TABLE IF EXISTS tmpr;

    CREATE TEMPORARY TABLE tmpl ENGINE=MEMORY 
    SELECT * FROM tranzactions_ordered 
    WHERE tr_caid = caid AND (tr_dateid BETWEEN first AND last);

    CREATE TEMPORARY TABLE tmpr ENGINE=MEMORY SELECT * FROM tmpl;

    CREATE TEMPORARY TABLE tmpall ENGINE=MEMORY 
    SELECT l.*, r.*,
        l_balance + r_sgn * r_amount = r_balance as valid
    FROM 
        (SELECT @i:=@i+1 AS l_row_id, tr_dateid as l_tr_dateid, tr_newbalance as l_balance FROM tmpl, (SELECT @i:=0) a) AS l, 
        (SELECT @j:=@j+1 AS r_row_id, tr_dateid as r_tr_dateid, tr_newbalance as r_balance, tr_amount as r_amount, tr_sgn as r_sgn FROM tmpr, (SELECT @j:=0) a) AS r 
    WHERE
        l_row_id + 1 = r_row_id; 

    SELECT
        l_tr_dateid, r_tr_dateid, 
        l_balance as "Previous balance", 
        r_amount as "Amount",
        r_sgn as "Sgn",
        r_balance as "New balance",
        IF(valid, "[ OK ]", "[fail]") as "[    ]" 
    FROM tmpall;

    SELECT COUNT(l_balance) = SUM(valid) FROM tmpall INTO is_valid;

    DROP TEMPORARY TABLE IF EXISTS tmpall;
    DROP TEMPORARY TABLE IF EXISTS tmpl;
    DROP TEMPORARY TABLE IF EXISTS tmpr;
END $$
DELIMITER ;

CALL test_update();

SELECT `prev`.`date`, `prev`.`amount`, `prev`.`balance`, `act`.`amount`, `act`.`balance`, `act`.`date`, `act`.`cluster`, `clusters`.`sgn` FROM 
    (SELECT @i:=@i+1 AS `row_id`, `amount`, `balance`, `date` FROM `view_pkez`, (SELECT @i:=0) a) AS `prev`, 
    (SELECT @j:=@j+1 AS `row_id`, `amount`, `balance`, `date`, `cluster` FROM `view_pkez`, (SELECT @j:=0) a) AS `act`
INNER JOIN `clusters` ON `act`.`cluster` = `clusters`.`name`
WHERE 
    `prev`.`row_id`+1 = `act`.`row_id`; 


SELECT `prev`.`date`, `prev`.`amount`, `prev`.`balance`, `act`.`amount`, `act`.`balance`, `act`.`date`, `act`.`cluster`, `clusters`.`sgn` FROM 
(   (SELECT @i:=@i+1 AS `row_id`, `amount`, `balance`, `date` FROM `view_pkez`, (SELECT @i:=0) a) AS `prev`
    INNER JOIN
    (SELECT @j:=@j+1 AS `row_id`, `amount`, `balance`, `date`, `cluster` FROM `view_pkez`, (SELECT @j:=0) a) AS `act`
    ON `prev`.`row_id`+1 = `act`.`row_id`)
INNER JOIN `clusters` ON `act`.`cluster` = `clusters`.`name`;















------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------












DROP VIEW IF EXISTS view_pkez;
CREATE VIEW view_pkez AS SELECT t_transactions.*, t_cluster.sgn AS 'sgn' FROM t_transactions INNER JOIN t_cluster ON t_cluster.name = t_transactions.cluster WHERE ca = 'pkez' ORDER BY date;
SELECT * FROM view_pkez;

-- DROP VIEW IF EXISTS view_pkez_paired;
-- CREATE VIEW view_pkez_paired AS 
SELECT prev.date, prev.amount, prev.balance, act.amount, act.balance, act.date, act.cluster, t_cluster.sgn FROM 
    (SELECT @i:=@i+1 AS row_id, amount, balance, date FROM view_pkez, (SELECT @i:=0) a) AS prev, 
    (SELECT @j:=@j+1 AS row_id, amount, balance, date, cluster FROM view_pkez, (SELECT @j:=0) a) AS act
INNER JOIN t_cluster ON act.cluster = t_cluster.name
WHERE 
    prev.row_id+1 = act.row_id; 


SELECT prev.date, prev.amount, prev.balance, act.amount, act.balance, act.date, act.cluster, t_cluster.sgn FROM 
(   (SELECT @i:=@i+1 AS row_id, amount, balance, date FROM view_pkez, (SELECT @i:=0) a) AS prev
    INNER JOIN
    (SELECT @j:=@j+1 AS row_id, amount, balance, date, cluster FROM view_pkez, (SELECT @j:=0) a) AS act
    ON prev.row_id+1 = act.row_id)
INNER JOIN t_cluster ON act.cluster = t_cluster.name;

SELECT amount, balance, sgn 
FROM t_transactions INNER JOIN t_cluster
ON t_cluster.name = t_transactions.cluster;


-- UPDATE t_transactions 
-- SET 
--     t_transactions.balance = other_table.balance
-- FROM
--     t_transactions
-- INNER JOIN
--     (SELECT * FROM t_transactions) as other_table
-- ON
--     t_transactions.id = other_table.id;


DROP FUNCTION IF EXISTS last_insert_rowid;
DELIMITER $$
CREATE FUNCTION last_insert_rowid()
    RETURNS INT
BEGIN
    DECLARE id INT;
    SELECT last_insert_id() INTO id;
    RETURN id;
END;
$$
DELIMITER ;

DROP FUNCTION IF EXISTS update_transactions;
DELIMITER $$
CREATE FUNCTION update_transactions()
    RETURNS INT READS SQL DATA
BEGIN
    DECLARE v_total INT;
    DECLARE v_counter INT;
    DECLARE done INT DEFAULT FALSE;
    DECLARE csr CURSOR FOR SELECT amount FROM t_transactions;
    DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;

  SET v_total = 0;
  OPEN csr;
  read_loop: LOOP
    FETCH csr INTO v_counter;

    IF done THEN
      LEAVE read_loop;
    END IF;

    SET v_total = v_total + v_counter;
  END LOOP;
  CLOSE csr;

  RETURN v_total;
END;
$$

DELIMITER ;


select result1.title1, title1.age1,result2.title2, title2.age2 from 
  (select @i:=@i+1 AS rowId, title1, age1 from tab1,(SELECT @i:=0) a) as result1, 
  (select @j:=@j+1 AS rowId, title2, age2 from tab2,(SELECT @j:=0) a) as result2 
where 
  result1.rowId = result2.rowId; 
-- try this it will work perfectly fine


DROP FUNCTION IF EXISTS update_transactions;
DROP PROCEDURE IF EXISTS update_transactions;
DELIMITER $$
CREATE PROCEDURE update_transactions(IN in_caid VARCHAR(8))
    -- RETURNS INT READS SQL DATA
BEGIN
    DECLARE trid INT;
    DECLARE old_balance INT;
    DECLARE new_balance INT;
    DECLARE day DATE;
    DECLARE amount INT;
    DECLARE act_balance INT;
    DECLARE sgn INT;

    DECLARE done INT DEFAULT FALSE;
    DECLARE csr CURSOR FOR 
        -- SELECT date, amount, balance, sgn 
        SELECT t_transactions.id, t_transactions.date, t_transactions.amount, t_transactions.balance, t_cluster.sgn
        FROM t_transactions INNER JOIN t_cluster ON t_cluster.name = t_transactions.cluster WHERE caid = in_caid ORDER BY date;
    DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;

    SET new_balance = 200000;
    SET old_balance = new_balance;
    OPEN csr;
    read_loop: LOOP
        FETCH csr INTO trid, day, amount, act_balance, sgn;
        SET new_balance = new_balance + sgn * amount;

        SELECT day, old_balance, amount, sgn, new_balance, act_balance;
        SET old_balance = new_balance;

        IF new_balance != act_balance THEN
            UPDATE t_transactions SET balance = new_balance WHERE id = trid;
        END IF;

        IF done THEN
            LEAVE read_loop;
        END IF;

    END LOOP;
    CLOSE csr;

    -- RETURN balance;
END;
$$

DELIMITER ;

CALL update_transactions('pkez');


DROP TRIGGER IF EXISTS tranzactions_before_insert_trigger;
CREATE DEFINER=root@localhost TRIGGER tranzactions_before_insert_trigger 
BEFORE INSERT ON tranzactions FOR EACH ROW 
BEGIN
    DECLARE sign INTEGER;
    DECLARE balance INTEGER;

    SELECT d.sign FROM direction as d WHERE NEW.dir = d.name INTO sign;
    SELECT ca.balance FROM charge_account AS ca WHERE NEW.caid = ca.id INTO balance;

    SET balance = balance + sign * NEW.amount;

    -- this is a simple tranzaction
    IF NEW.caid_tofrom = 'out' THEN
    
        UPDATE charge_account AS ca SET 
            ca.balance = balance,
            ca.date = SYSDATE()
        WHERE ca.id = NEW.caid;

    ELSE -- move tranzaction (e.g. from potp to pkez)

        -- subtract from the actual charge account
        UPDATE charge_account AS ca SET
            ca.balance = balance,
            ca.date = SYSDATE()
        WHERE ca.id = new.caid;

        -- add to the other charge account
        UPDATE charge_account AS ca SET
            ca.balance = ca.balance - sign * new.amount,
            ca.date = SYSDATE()
        WHERE ca.id = new.caid_tofrom;

    END IF;

    IF NEW.date IS NULL THEN
        SET NEW.date = SYSDATE();
    END IF;
    
    IF NEW.balance = -1 THEN
        SET NEW.balance = balance;
    END IF;
END

-- ez jol mukodik
DROP PROCEDURE IF EXISTS get_last_transaction;
DELIMITER $$
CREATE PROCEDURE get_last_transaction(IN in_tr_date DATE, IN in_tr_caid VARCHAR(8))
SQL SECURITY DEFINER
BEGIN
    SELECT * FROM tranzactions WHERE date < in_tr_date AND caid = in_tr_caid ORDER BY date DESC LIMIT 1;
END;
$$
DELIMITER ;


DROP TRIGGER IF EXISTS tranzactions_before_insert_trigger;
CREATE DEFINER=root@localhost TRIGGER tranzactions_before_insert_trigger 
BEFORE INSERT ON tranzactions FOR EACH ROW 
BEGIN
    DECLARE sign INTEGER;
    DECLARE balance INTEGER;

    SELECT d.sign FROM direction as d WHERE NEW.dir = d.name INTO sign;
    SELECT ca.balance FROM charge_account AS ca WHERE NEW.caid = ca.id INTO balance;

    SET balance = balance + sign * NEW.amount;

    -- this is a simple tranzaction
    IF NEW.caid_tofrom = 'out' THEN
    
        UPDATE charge_account AS ca SET 
            ca.balance = balance,
            ca.date = SYSDATE()
        WHERE ca.id = NEW.caid;

    ELSE -- move tranzaction (e.g. from potp to pkez)

        -- subtract from the actual charge account
        UPDATE charge_account AS ca SET
            ca.balance = balance,
            ca.date = SYSDATE()
        WHERE ca.id = new.caid;

        -- add to the other charge account
        UPDATE charge_account AS ca SET
            ca.balance = ca.balance - sign * new.amount,
            ca.date = SYSDATE()
        WHERE ca.id = new.caid_tofrom;

    END IF;

    IF NEW.date IS NULL THEN
        SET NEW.date = SYSDATE();
    END IF;
    
    IF NEW.balance = -1 THEN
        SET NEW.balance = balance;
    END IF;
END













-- HJ beadandohoz kellett:
create table customer(
uid        int not null auto_increment,
username   varchar(50) ,
firstname  varchar(255),
lastname   varchar(255),
password   varchar(50) , 
role       varchar(50) ,
PRIMARY KEY(uid));

create table product(
uid         int not null auto_increment,
productname varchar(255)               ,
price       int                        ,
PRIMARY KEY(uid));

create table purchase(
uid         int not null auto_increment,
order_date  date                       ,
customer_id int                        ,
PRIMARY KEY(uid));

create table purchase2product(
purchase_id int,
product_id  int
)