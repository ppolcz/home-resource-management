package org.ppke.itk.hj15.service;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import org.ppke.itk.hj15.interfaces.CustomerServiceLocal;
import org.ppke.itk.hj15.interfaces.OrderServiceLocal;
import org.ppke.itk.hj15.model.Customer;
import org.ppke.itk.hj15.model.Order;
import org.ppke.itk.hj15.model.Product;

@Stateless
public class OrderService implements OrderServiceLocal {
	
	@PersistenceContext
	private EntityManager entityManager;

	
	@EJB
	private CustomerServiceLocal customerService;

	@Override
	public void addOrder(String userName, String firstName, String lastName, List<Product> products) {
		
		Customer customer = customerService.addCustomer(userName, lastName, firstName);
		Order order = new Order();
		order.setCustomer(customer);
		for(Product product : products){
			order.getProducts().add(entityManager.merge(product));
		}
		entityManager.merge(order);

	}

	@Override
	public List<Order> getOrders(String username) {
		TypedQuery<Order> query = entityManager.createQuery("SELECT o FROM Order o join fetch o.customer left join fetch o.products", Order.class);
		return query.getResultList();
	}
	
	@Override
	public List<Order> getOrders() {
		TypedQuery<Order> query = entityManager.createQuery("SELECT o FROM Order o join fetch o.customer left join fetch o.products", Order.class);
		return query.getResultList();
	}

}
