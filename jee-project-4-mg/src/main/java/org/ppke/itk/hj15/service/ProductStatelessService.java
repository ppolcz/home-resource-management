package org.ppke.itk.hj15.service;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.ejb.Schedule;
import javax.ejb.Stateless;
import javax.interceptor.Interceptors;

import org.ppke.itk.hj15.interfaces.ProductStatelessServiceLocal;
import org.ppke.itk.hj15.model.Product;
import org.ppke.itk.hj15.util.MethodLogger;


import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

@Stateless
@Interceptors({MethodLogger.class})
public class ProductStatelessService implements Serializable, ProductStatelessServiceLocal {

	private static final long serialVersionUID = -1L;
	
	List<Product> products;
	//TODO megvalósítani az interfész függvényeket + annotációt elhelyezni
	
	@PostConstruct
	public void postConstruct(){
	   System.out.println("Stateless bean initialized");
	   products = new ArrayList<>();
	   addProduct("Iphone 6S", new Random().nextDouble()*100000);
	}
	
	@Override
	public void addProduct(String name, Double price) {
		products.add(new Product(name, price));
		
	}
	
	@Override
	public List<Product> getProducts() {
			return products;
	}


	
	@PreDestroy
	public void preDestroy(){
		System.out.println("Stateless bean being destroyed");
	}
	


}