package org.ppke.itk.hj15.util;

import javax.interceptor.AroundInvoke;
import javax.interceptor.InvocationContext;

public class MethodLogger {
	
	public MethodLogger(){
		
	}
	
	@AroundInvoke
	public Object logMethodInvocationTime(InvocationContext context){
		
	
		long start = System.currentTimeMillis();
		
		try {
			
			
			return context.proceed();
			
			
		} catch (Exception e) {
			
			e.printStackTrace();
		}finally{
		   long end = System.currentTimeMillis();
		   long length = end - start;
		   System.out.println("Length of method "+ context.getMethod().getName()+" :" + length +" ms");
		}
		
		
		return null;
		
	}
	

}
