package org.ppke.itk.hj15.service;

import javax.ejb.Schedule;
import javax.ejb.Singleton;
import javax.ejb.Startup;


@Singleton
@Startup
public class WebshopSingletonService {

    @Schedule(second="*/20", minute = "*", hour = "*")
    public void logHelloWorld(){
        System.out.println("Hello World!");
    }
}
