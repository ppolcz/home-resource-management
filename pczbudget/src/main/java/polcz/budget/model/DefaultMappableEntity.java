package polcz.budget.model;

import java.io.Serializable;

public abstract class DefaultMappableEntity implements MappableEntity<Integer, String>, Serializable
{
    private static final long serialVersionUID = 3009894407867977204L;

    @Override
    public boolean equals(Object obj)
    {

        if (obj instanceof DefaultMappableEntity) return this.getClass().equals(obj.getClass())
            && this.getKey() == ((DefaultMappableEntity) obj).getKey();
        return false;
    }

    @Override
    public String toString()
    {
        return this.getClass().getSimpleName() + "::" + getValue();
    }

    @Override
    public int hashCode()
    {
        return getKey();
    }
}
