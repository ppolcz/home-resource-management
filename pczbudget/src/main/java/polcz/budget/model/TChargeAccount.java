package polcz.budget.model;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;


/**
 * The persistent class for the t_charge_accounts database table.
 * 
 */
@Entity
@Table(name="t_charge_accounts")
@NamedQueries(value = {
		@NamedQuery(name = "TChargeAccount.findByName", query = "SELECT e FROM TChargeAccount e where e.name = :name"),
		@NamedQuery(name = "TChargeAccount.findAll", query = "SELECT e FROM TChargeAccount e") })
public class TChargeAccount extends DefaultMappableEntity {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(unique=true, nullable=false)
	private int uid;

	@Column(name="description", length=100)
	private String desc;

	@Column(unique=true, nullable=false, length=8)
	private String name;

	// TODO ezekkel csinaljak mar valamit:

	//bi-directional many-to-one association to TTransaction
	@OneToMany(mappedBy="ca", fetch=FetchType.LAZY)
	private List<TTransaction> TTransactions;

	public TChargeAccount() {
	}

	public TChargeAccount(String name, String desc) {
		this.name = name;
		this.desc = desc;
	}

	public TChargeAccount(String name) {
		this.name = name;
	}

	public int getUid() {
		return this.uid;
	}

	public TChargeAccount setUid(int uid) {
		this.uid = uid;
		return this;
	}

	public String getDesc() {
		return this.desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

	public String getName() {
		return this.name;
	}

	public TChargeAccount setName(String name) {
		this.name = name;
		return this;
	}

	public List<TTransaction> getTTransactions() {
		return this.TTransactions;
	}

	public void setTTransactions(List<TTransaction> TTransactions) {
		this.TTransactions = TTransactions;
	}

	public TTransaction addTTransaction(TTransaction TTransaction) {
		getTTransactions().add(TTransaction);
		TTransaction.setCa(this);

		return TTransaction;
	}

	public TTransaction removeTTransaction(TTransaction TTransaction) {
		getTTransactions().remove(TTransaction);
		TTransaction.setCa(null);

		return TTransaction;
	}

	@Override
	public Integer getKey() {
		return getUid();
	}

	@Override
	public String getValue() {
		return getName();
	}

	@Override
	public MappableEntity<Integer, String> setKey(Integer k) {
		return setUid(k);
	}

	@Override
	public MappableEntity<Integer, String> setValue(String v) {
		return setName(v);
	}

	public boolean equals(TChargeAccount obj) {
		return obj.getUid() == uid;
	}
}