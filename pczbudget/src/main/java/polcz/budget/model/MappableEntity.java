package polcz.budget.model;

public interface MappableEntity<K,V> {
	K getKey();
	V getValue();
	
	MappableEntity<K, V> setKey(K k);
	MappableEntity<K, V> setValue(V v);
}
