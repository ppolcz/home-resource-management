package polcz.budget.model;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import polcz.util.Util;

/**
 * The persistent class for the t_transactions database table.
 */
@Entity
@Table(name = "t_transactions")
@NamedQueries(value = {
    @NamedQuery(name = "TTransaction.findOne", query = "SELECT e FROM TTransaction e where e.uid = :uid"),
    @NamedQuery(name = "TTransaction.findAll", query = "SELECT e FROM TTransaction e") })
public class TTransaction implements Serializable
{
    private static final long serialVersionUID = 1L;

    @Id
    @Column(unique = true, nullable = false)
    private int uid;

    @Column(nullable = false)
    private int amount;

    @Column(nullable = false)
    private int balance;

    @Temporal(TemporalType.DATE)
    @Column(nullable = false)
    private Date date;

    @Column(length = 100)
    private String remark;

    @Column(name = "pivot", nullable = false)
    private boolean pivot;

    // bi-directional many-to-one association to TProductInfo
    @OneToMany(mappedBy = "transaction", fetch = FetchType.LAZY)
    private List<TProductInfo> pis;

    // bi-directional many-to-one association to TChargeAccount
    @ManyToOne
    @JoinColumn(name = "ca", nullable = false)
    private TChargeAccount ca;

    @ManyToOne
    @JoinColumn(name = "catransfer", nullable = false)
    private TChargeAccount catransfer;

    // bi-directional many-to-one association to TCluster
    @ManyToOne
    @JoinColumn(name = "cluster", nullable = false)
    private TCluster cluster;

    // bi-directional many-to-one association to TMarket
    @ManyToOne
    @JoinColumn(name = "market")
    private TMarket market;

    public TTransaction()
    {}

    public TTransaction(int amount, int balance, Date date, String remark, TChargeAccount ca, TCluster cluster,
        TMarket market, boolean pivot)
    {
        this.amount = amount;
        this.balance = balance;
        this.date = date;
        this.remark = remark;
        this.ca = ca;
        this.cluster = cluster;
        this.market = market;
        this.pivot = pivot;
    }

    @Override
    public String toString()
    {
        return String.format("{ %d, %s, %s | %8d, %8d %16.16s (%2d) | from:%s to:%s | mk:%16.16s | remark: %s }", uid,
            Util.SIMPLE_DATE_FORMAT.format(date),
            pivot ? "PIVOT" : "     ", amount, balance,
            cluster == null ? null : cluster.getName(),
            cluster == null ? 0 : cluster.getSgn(),
            ca == null ? null : ca.getName(),
            catransfer == null ? null : catransfer.getName(),
            market == null ? null : market.getName(),
            remark == null ? null : remark);
        // + "\ntoString = [" + super.toString() + "]";
    }

    public int getUid()
    {
        return this.uid;
    }

    public TTransaction setUid(int uid)
    {
        this.uid = uid;
        return this;
    }

    public int getAmount()
    {
        return this.amount;
    }

    public void setAmount(int amount)
    {
        this.amount = amount;
    }

    public int getBalance()
    {
        return this.balance;
    }

    public void setBalance(int balance)
    {
        this.balance = balance;
    }

    public Date getDate()
    {
        return this.date;
    }

    public void setDate(Date date)
    {
        this.date = date;
    }

    public String getRemark()
    {
        return this.remark;
    }

    public void setRemark(String remark)
    {
        this.remark = remark;
    }

    public List<TProductInfo> getTProductInfos()
    {
        return this.pis;
    }

    public void setTProductInfos(List<TProductInfo> TProductInfos)
    {
        this.pis = TProductInfos;
    }

    public TProductInfo addTProductInfo(TProductInfo TProductInfo)
    {
        getTProductInfos().add(TProductInfo);
        TProductInfo.setTTransaction(this);

        return TProductInfo;
    }

    public TProductInfo removeTProductInfo(TProductInfo TProductInfo)
    {
        getTProductInfos().remove(TProductInfo);
        TProductInfo.setTTransaction(null);

        return TProductInfo;
    }

    public TChargeAccount getCa()
    {
        return this.ca;
    }

    public void setCa(TChargeAccount TChargeAccount)
    {
        this.ca = TChargeAccount;
    }

    public TCluster getCluster()
    {
        return this.cluster;
    }

    public void setCluster(TCluster TCluster)
    {
        this.cluster = TCluster;
    }

    public TMarket getMarket()
    {
        return this.market;
    }

    public void setMarket(TMarket TMarket)
    {
        this.market = TMarket;
    }

    public boolean isPivot()
    {
        return pivot;
    }

    public void setPivot(boolean pivot)
    {
        this.pivot = pivot;
    }

    public TChargeAccount getCatransfer()
    {
        return catransfer;
    }

    public TTransaction setCatransfer(TChargeAccount catransfer)
    {
        this.catransfer = catransfer;
        return this;
    }
}