package polcz.budget.model;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * The persistent class for the t_cluster database table.
 * 
 */
@Entity
@Table(name = "t_cluster")
@NamedQueries(value = { @NamedQuery(name = "TCluster.findOne", query = "SELECT e FROM TCluster e where e.uid = :uid"),
		@NamedQuery(name = "TCluster.findByName", query = "SELECT e FROM TCluster e where e.name = :name"),
		@NamedQuery(name = "TCluster.findAll", query = "SELECT e FROM TCluster e") })
public class TCluster extends DefaultMappableEntity {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(unique = true, nullable = false)
	private int uid;

	@Column(unique = true, nullable = false, length = 45)
	private String name;

	@Column(nullable = false)
	private int sgn;

	// bi-directional many-to-one association to TCluster
	@ManyToOne
	@JoinColumn(name = "parent")
	private TCluster parent;

	// bi-directional many-to-one association to TCluster
	@OneToMany(mappedBy = "parent", fetch = FetchType.LAZY)
	private List<TCluster> children;

	// TODO ezekkel csinaljak mar valamit:

	// bi-directional many-to-one association to TProductInfo
	@OneToMany(mappedBy = "cluster", fetch = FetchType.LAZY)
	private List<TProductInfo> pis;

	// bi-directional many-to-one association to TTransaction
	@OneToMany(mappedBy = "cluster", fetch = FetchType.LAZY)
	private List<TTransaction> trs;

	public TCluster() {
	}

	public TCluster(String name, int sgn, TCluster parent) {
		this.name = name;
		this.sgn = sgn;
		this.parent = parent;
	}

	public TCluster(String name, TCluster parent) {
		this.name = name;
		this.sgn = parent.sgn;
		this.parent = parent;
	}

	public TCluster(String name) {
		this.name = name;
	}

	public int getUid() {
		return this.uid;
	}

	public TCluster setUid(int uid) {
		this.uid = uid;
		return this;
	}

	public String getName() {
		return this.name;
	}

	public TCluster setName(String name) {
		this.name = name;
		return this;
	}

	public int getSgn() {
		return this.sgn;
	}

	public TCluster setSgn(int sgn) {
		this.sgn = sgn;
		return this;
	}

	public TCluster getParent() {
		return this.parent;
	}

	public TCluster setParent(TCluster TCluster) {
		this.parent = TCluster;
		return this;
	}

	public List<TCluster> getTClusters() {
		return this.children;
	}

	public void setChildren(List<TCluster> children) {
		this.children = children;
	}

	public TCluster addChild(TCluster child) {
		getTClusters().add(child);
		child.setParent(this);

		return child;
	}

	public TCluster removeChild(TCluster child) {
		getTClusters().remove(child);
		child.setParent(null);

		return child;
	}

	public List<TProductInfo> getTProductInfos() {
		return this.pis;
	}

	public void setTProductInfos(List<TProductInfo> TProductInfos) {
		this.pis = TProductInfos;
	}

	public TProductInfo addTProductInfo(TProductInfo TProductInfo) {
		getTProductInfos().add(TProductInfo);
		TProductInfo.setTCluster(this);

		return TProductInfo;
	}

	public TProductInfo removeTProductInfo(TProductInfo TProductInfo) {
		getTProductInfos().remove(TProductInfo);
		TProductInfo.setTCluster(null);

		return TProductInfo;
	}

	public List<TTransaction> getTTransactions() {
		return this.trs;
	}

	public void setTTransactions(List<TTransaction> TTransactions) {
		this.trs = TTransactions;
	}

	public TTransaction addTTransaction(TTransaction TTransaction) {
		getTTransactions().add(TTransaction);
		TTransaction.setCluster(this);

		return TTransaction;
	}

	public TTransaction removeTTransaction(TTransaction TTransaction) {
		getTTransactions().remove(TTransaction);
		TTransaction.setCluster(null);

		return TTransaction;
	}

	@Override
	public Integer getKey() {
		return getUid();
	}

	@Override
	public String getValue() {
		return getName();
	}

	@Override
	public MappableEntity<Integer, String> setKey(Integer k) {
		return setUid(k);
	}

	@Override
	public MappableEntity<Integer, String> setValue(String v) {
		return setName(v);
	}

	public boolean equals(TCluster obj) {
		return obj.getUid() == uid;
	}

}