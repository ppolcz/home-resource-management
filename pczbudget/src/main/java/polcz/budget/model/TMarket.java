package polcz.budget.model;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * The persistent class for the t_markets database table.
 * 
 */
@Entity
@Table(name = "t_markets")
@NamedQueries(value = { @NamedQuery(name = "TMarket.findOne", query = "SELECT e FROM TMarket e where e.uid = :uid"),
		@NamedQuery(name = "TMarket.findByName", query = "SELECT e FROM TMarket e where e.name = :name"),
		@NamedQuery(name = "TMarket.findAll", query = "SELECT e FROM TMarket e") })
public class TMarket extends DefaultMappableEntity {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(unique = true, nullable = false)
	private int uid;

	@Column(name = "description", length = 256)
	private String desc;

	@Column(unique = true, nullable = false, length = 45)
	private String name;

	// bi-directional many-to-one association to TProductInfo
	@OneToMany(mappedBy = "market", fetch = FetchType.LAZY)
	private List<TProductInfo> TProductInfos;

	// bi-directional many-to-one association to TTransaction
	@OneToMany(mappedBy = "market", fetch = FetchType.LAZY)
	private List<TTransaction> trs; // TODO

	public TMarket() {
	}

	public TMarket(String name, String desc) {
		this.name = name;
		this.desc = desc;
	}

	public int getUid() {
		return this.uid;
	}

	public TMarket setUid(int uid) {
		this.uid = uid;
		return this;
	}

	public String getDesc() {
		return this.desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

	public String getName() {
		return this.name;
	}

	public TMarket setName(String name) {
		this.name = name;
		return this;
	}

	public List<TProductInfo> getTProductInfos() {
		return this.TProductInfos;
	}

	public void setTProductInfos(List<TProductInfo> TProductInfos) {
		this.TProductInfos = TProductInfos;
	}

	public TProductInfo addTProductInfo(TProductInfo TProductInfo) {
		getTProductInfos().add(TProductInfo);
		TProductInfo.setTMarket(this);

		return TProductInfo;
	}

	public TProductInfo removeTProductInfo(TProductInfo TProductInfo) {
		getTProductInfos().remove(TProductInfo);
		TProductInfo.setTMarket(null);

		return TProductInfo;
	}

	public List<TTransaction> getTTransactions() {
		return this.trs;
	}

	public void setTTransactions(List<TTransaction> TTransactions) {
		this.trs = TTransactions;
	}

	public TTransaction addTTransaction(TTransaction TTransaction) {
		getTTransactions().add(TTransaction);
		TTransaction.setMarket(this);

		return TTransaction;
	}

	public TTransaction removeTTransaction(TTransaction TTransaction) {
		getTTransactions().remove(TTransaction);
		TTransaction.setMarket(null);

		return TTransaction;
	}

	@Override
	public Integer getKey() {
		return getUid();
	}

	@Override
	public String getValue() {
		return getName();
	}

	@Override
	public MappableEntity<Integer, String> setKey(Integer k) {
		return setUid(k);
	}

	@Override
	public MappableEntity<Integer, String> setValue(String v) {
		return setName(v);
	}
    //
    // public boolean equals(TMarket obj) {
    // return obj.getUid() == uid;
    // }
    //
    // @Override
    // public String toString()
    // {
    // return "TMarket::" + getName();
    // }
    //
    // @Override
    // public int hashCode()
    // {
    // return getUid();
    // }
    //
    // @Override
    // public boolean equals(Object obj)
    // {
    // if (obj instanceof TMarket)
    // {
    // return ((TMarket) obj).getUid() == getUid();
    // }
    // return false;
    // }
}