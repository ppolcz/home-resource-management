package polcz.budget.model;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the t_product_infos database table.
 * 
 */
@Entity
@Table(name="t_product_infos")
@NamedQueries(value = {
		@NamedQuery(name = "TProductInfo.findOne", query = "SELECT e FROM TProductInfo e where e.uid = :uid"),
		@NamedQuery(name = "TProductInfo.findAll", query = "SELECT e FROM TProductInfo e") })
public class TProductInfo implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(unique=true, nullable=false)
	private int uid;

	@Column(nullable=false)
	private int amount;

	@Column(name="amount_orig")
	private int amountOrig;

	@Column(length=8)
	private String currency;

	@Column(name="description", nullable=false, length=300)
	private String desc;

	//bi-directional many-to-one association to TCluster
	@ManyToOne
	@JoinColumn(name="cluster", nullable=false)
	private TCluster cluster;

	//bi-directional many-to-one association to TMarket
	@ManyToOne
	@JoinColumn(name="market", nullable=false)
	private TMarket market;

	//bi-directional many-to-one association to TTransaction
	@ManyToOne
	@JoinColumn(name="trid")
	private TTransaction transaction;

	public TProductInfo() {
	}

	public int getUid() {
		return this.uid;
	}

	public void setUid(int uid) {
		this.uid = uid;
	}

	public int getAmount() {
		return this.amount;
	}

	public void setAmount(int amount) {
		this.amount = amount;
	}

	public int getAmountOrig() {
		return this.amountOrig;
	}

	public void setAmountOrig(int amountOrig) {
		this.amountOrig = amountOrig;
	}

	public String getCurrency() {
		return this.currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public String getDesc() {
		return this.desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

	public TCluster getTCluster() {
		return this.cluster;
	}

	public void setTCluster(TCluster TCluster) {
		this.cluster = TCluster;
	}

	public TMarket getTMarket() {
		return this.market;
	}

	public void setTMarket(TMarket TMarket) {
		this.market = TMarket;
	}

	public TTransaction getTTransaction() {
		return this.transaction;
	}

	public void setTTransaction(TTransaction TTransaction) {
		this.transaction = TTransaction;
	}

}