package polcz.budget.model;

public enum TTransactionType {
	simple,
	transfer,
	pivot;
//	SIMPLE_TRANSACTION,
//	TRANSFER_TRANSACTION,
//	PIVOT_TRANSACTION
	
	public String getName() {
		return this.name();
	}
}
