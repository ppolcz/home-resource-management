package polcz.budget.global;

import org.jboss.logging.Logger;

public class R
{
    public static final String MK_UID = "mk_uid";
    public static final String CL_UID = "cl_uid";
    public static final String CA_UID = "ca_uid";
    public static final String CAT_UID = "ca_transfer_uid";
    
    public static final String MK_PREFIX = "mk_";
    public static final String CA_PREFIX = "ca_";
    public static final String CL_PREFIX = "cl_";    
    
    public static final String GLOBAL_MSG_ID = "globalMessage";
    public static final String REDIRECT = "?faces-redirect=true";
    public static final String REDIRECT_VIEW = "view" + REDIRECT;
    public static final String REDIRECT_CREATE = "create" + REDIRECT;
    public static final String REDIRECT_EDIT = "edit" + REDIRECT;

    public static final int TR_REMOVAL = 1;
    public static final int TR_INSERTION = 2;
    public static final int TR_UPDATE = 3;

    public static final String LOGGER_PREFIX = "PPOLCZ_";
    public static final String LOGGER_SUFFIX = "_PPOLCZ";
    
    private R()
    {}

    public static Logger getJBossLogger(Class<?> c)
    {
        return Logger.getLogger(c, LOGGER_SUFFIX);
    }
    
    public static java.util.logging.Logger getUtilLogger(Class<?> c)
    {
        return java.util.logging.Logger.getLogger(LOGGER_PREFIX, c.getSimpleName());
    }
}
