package polcz.budget.session;

import java.io.Serializable;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import polcz.budget.model.TMarket;
import polcz.budget.service.AbstractService;
import polcz.budget.service.MarketService;
import polcz.budget.session.helper.SimpleMapBean;

@ManagedBean(name="mkBean")
@SessionScoped
public class MarketBean extends SimpleMapBean<TMarket, Integer, String> implements Serializable {

	public MarketBean() {
		super(MarketBean.class);
	}

	private static final long serialVersionUID = 2286250981768653297L;

	@EJB
	private MarketService service;

	@Override
	public AbstractService<TMarket> getService() {
		return service;
	}

	@Override
	public TMarket instance() {
		return new TMarket(getName(), getDesc());
	}

	@Override
	public TMarket instance(int uid) {
		return instance().setUid(uid);
	}

}
