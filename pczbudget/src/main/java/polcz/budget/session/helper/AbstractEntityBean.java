package polcz.budget.session.helper;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import javax.annotation.PostConstruct;
import javax.faces.event.ActionEvent;
import javax.faces.event.AjaxBehaviorEvent;
import javax.faces.event.ComponentSystemEvent;
import javax.faces.event.ValueChangeEvent;

import org.jboss.logging.Logger;

import polcz.budget.service.AbstractService;

public abstract class AbstractEntityBean<T> implements EntityBeanHelperMethods<T, AbstractService<T>> {

	private int ID = new Random(System.currentTimeMillis()).nextInt();

	public int getID() {
		return ID;
	}

	protected List<T> list = new ArrayList<>();
	protected int uid;

	// private Class<?> childClass;

	protected Logger logger;

	protected void log(String format, Object... objs)
	{
	    logger.info(String.format(format, objs));
	}
	
	protected AbstractEntityBean(Class<?> childClass) {
		// this.childClass = childClass;
		logger = Logger.getLogger("PPOLCZ_" + childClass.getSimpleName());
		logger.info("SESSION BEAN INSTATIATED: " + childClass.getSimpleName());
	}

	public String persist() {
		getService().create(instance());
		updateList();
		return "view?faces-redirect=true";
	}

	public String merge() {
		getService().edit(instance(uid));
		updateList();
		return "view?faces-redirect=true";
	}
	
	public String remove(T e) {
	    getService().remove(e);
	    updateList();
        return "view?faces-redirect=true";
	}

	public int getUid() {
		return uid;
	}

	public void setUid(int uid) {
		this.uid = uid;
	}

	/* EGY KIS GYORSITAS - NE KERDEZZEN LE FOLOSLEGESEN
	 * http://stackoverflow.com/questions/2090033/why-jsf-calls-getters-multiple-times */
	
	@PostConstruct
	public void init() {
		updateList();
    	logger.info(AbstractEntityBean.class.getSimpleName() + "::init() @PostConstruct");
	}
	
	public List<T> updateList() {
		list = getService().findAll();
		logger.info(this.getClass().getSimpleName() + "::updateList(), length = " + list.size());
		return list;		
	}
	
	public void   preRender(ComponentSystemEvent event) {
        // Or in some SystemEvent method (e.g. <f:event type="preRenderView">).
        updateList();
        logger.info(AbstractEntityBean.class.getSimpleName() + "::preRender()");
    }           

    public void   change(ValueChangeEvent event) {
        // Or in some FacesEvent method (e.g. <h:inputXxx valueChangeListener>).
        updateList();
    	logger.info(AbstractEntityBean.class.getSimpleName() + "::change()");
    }

    public void   ajaxListener(AjaxBehaviorEvent event) {
        // Or in some BehaviorEvent method (e.g. <f:ajax listener>).
        updateList();
    	logger.info(AbstractEntityBean.class.getSimpleName() + "::ajaxListener()");
    }

    public void   actionListener(ActionEvent event) {
        // Or in some ActionEvent method (e.g. <h:commandXxx actionListener>).
        updateList();
    	logger.info(AbstractEntityBean.class.getSimpleName() + "::actionListener()");
    }

    public String submit() {
        // Or in Action method (e.g. <h:commandXxx action>).
        updateList();
    	logger.info(AbstractEntityBean.class.getSimpleName() + "::submit()");
        return "outcome";
    }

	public List<T> getList() {
		return list;
	}

}
