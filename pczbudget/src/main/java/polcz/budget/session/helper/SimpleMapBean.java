package polcz.budget.session.helper;

import java.io.Serializable;
import java.util.Map;
import java.util.TreeMap;

import polcz.budget.global.R;
import polcz.budget.model.MappableEntity;

public abstract class SimpleMapBean<T extends MappableEntity<K, V>, K, V> extends AbstractEntityBean<T>
    implements Serializable
{

    private Map<V, T> items = new TreeMap<>();

    public SimpleMapBean(Class<?> childClass)
    {
        super(childClass);
    }

    private static final long serialVersionUID = 1123123L;

    private String name;
    private String desc;

    public String create()
    {
        name = null;
        desc = null;
        return R.REDIRECT_CREATE;
    }

    public String edit(int uid, String name, String desc)
    {
        logger.info(String.format("uid = %d, name = %s, desc = %s", uid, name, desc));
        this.uid = uid;
        this.name = name;
        this.desc = desc;
        return R.REDIRECT_EDIT;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public String getDesc()
    {
        return desc;
    }

    public void setDesc(String desc)
    {
        this.desc = desc;
    }

    public Map<V, T> getItems()
    {
        if (items.size() != getList().size())
        {
            items.clear();
            for (T o : getList())
                items.put(o.getValue(), o);
            logger.info("selectOneMenu items regenerated");
            logger.info("itemsNew.length [after] = " + items.size());
        }
        return items;
    }

}
