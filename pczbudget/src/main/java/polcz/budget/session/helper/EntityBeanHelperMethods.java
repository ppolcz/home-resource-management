package polcz.budget.session.helper;

import java.util.List;

import polcz.budget.service.AbstractService;

public interface EntityBeanHelperMethods<T,S extends AbstractService<T>> {
	T instance();
	T instance(int uid);
	S getService();
	List<T> getList();
}
