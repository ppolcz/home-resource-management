package polcz.budget.session;

import java.io.Serializable;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import polcz.budget.model.TChargeAccount;
import polcz.budget.service.AbstractService;
import polcz.budget.service.CAService;
import polcz.budget.session.helper.SimpleMapBean;

@ManagedBean(name="caBean")
@SessionScoped
public class ChargeAccountBean extends SimpleMapBean<TChargeAccount, Integer, String> implements Serializable {

	
	public ChargeAccountBean() {
		super(ChargeAccountBean.class);
	}

	private static final long serialVersionUID = 2286250981768653297L;

	@EJB
	private CAService service;

	@Override
	public AbstractService<TChargeAccount> getService() {
		return service;
	}

	@Override
	public TChargeAccount instance() {
		return new TChargeAccount(getName(), getDesc());
	}

	@Override
	public TChargeAccount instance(int uid) {
		return instance().setUid(uid);
	}

}
