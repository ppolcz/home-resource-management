package polcz.budget.session;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Random;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.event.AjaxBehaviorEvent;

import org.junit.Assert;
import org.primefaces.component.datatable.DataTable;
import org.primefaces.event.RowEditEvent;

import polcz.budget.global.R;
import polcz.budget.model.TChargeAccount;
import polcz.budget.model.TCluster;
import polcz.budget.model.TMarket;
import polcz.budget.model.TTransaction;
import polcz.budget.model.TTransactionType;
import polcz.budget.service.AbstractService;
import polcz.budget.service.CAService;
import polcz.budget.service.ClusterService;
import polcz.budget.service.MarketService;
import polcz.budget.service.StartupService;
import polcz.budget.service.TransactionService;
import polcz.budget.service.helper.TransactionArguments;
import polcz.budget.session.helper.AbstractEntityBean;

/**
 * Ez borzasztoan bonyolultra sikerult, atgondolast igenyel
 */
@ManagedBean
@SessionScoped
public class TransactionBean extends AbstractEntityBean<TTransaction> implements Serializable
{

    private static final long serialVersionUID = -8931274643248777827L;
    // @Getter
    private final int ID = new Random(System.currentTimeMillis()).nextInt();

    /// @formatter:off
    @EJB private StartupService ss;
    @EJB private TransactionService trservice;
    @EJB private CAService caservice;
    @EJB private MarketService mkservice;
    @EJB private ClusterService clservice;
    /// @formatter:on

    // @ManagedProperty(value = "#{mecBean}")
    // private MapEntityConverter mecBean;

    /* edit or create actual transaction entry */
    private Date date;
    private TChargeAccount ca;
    private TChargeAccount catransfer;
    private int amount;
    private int balance;
    private TCluster cluster;
    private TMarket market;
    private String remark;
    private boolean pivot;
    // private String remarkExtra;
    // private Integer balanceTmp;

    /* type of modification: insertion, update or removal */
    private TTransactionType type;

    /* the older transaction, which is going to be updated [type == update] */
    private TTransaction old;

    /* filter parameters */
    private TChargeAccount filterCa;
    private TChargeAccount filterCatransfer;
    private TCluster filterCluster;
    private TMarket filterMarket;

    public TransactionBean()
    {
        super(TransactionBean.class);
    }

    @PostConstruct
    public void init()
    {
        super.init();
        // filterCa = ss.pkez();
        Assert.assertNotNull(ss.CA_None());
    }

    public TTransactionType[] getTypes()
    {
        return TTransactionType.values();
    }

    public String create()
    {
        date = new Date(); /* today */
        amount = 0;
        balance = 0;
        ca = ss.pkez();
        cluster = ss.Napi_Szukseglet();
        market = ss.Market_Not_Applicable();
        remark = null;
        pivot = false;
        catransfer = ss.CA_None();
        type = TTransactionType.simple;

        log(String.format("trDate = %s", new SimpleDateFormat("yyyy-MM-dd").format(date)));
        return R.REDIRECT_CREATE;
    }

    public String edit(TTransaction tr)
    {
        log("edit transaction: %s", tr);

        uid = tr.getUid();
        date = tr.getDate();
        amount = tr.getAmount();
        balance = tr.getBalance();
        remark = tr.getRemark();
        pivot = tr.isPivot();
        ca = tr.getCa();
        catransfer = tr.getCatransfer();
        cluster = tr.getCluster();
        market = tr.getMarket();

        Assert.assertNotNull("ca is null", ca);
        Assert.assertNotNull("catransfer is null", catransfer);
        Assert.assertNotNull("market is null", market);
        Assert.assertNotNull("cluster is null", cluster);

        if (pivot)
        {
            Assert.assertTrue(String.format("in pivot case, the catransfer should be 'none'. "
                + "Expected: %s, got %s", ss.CA_None(), catransfer),
                ss.CA_None().equals(catransfer));

            type = TTransactionType.pivot;
        }
        else if (catransfer.equals(ss.CA_None()))
        {
            type = TTransactionType.simple;
        }
        else
        {
            type = TTransactionType.transfer;
        }

        /* here I store the old values of the selected transaction item */
        // TODO: tr.clone()
        old = new TTransaction();
        old.setCa(ca);
        old.setCatransfer(catransfer);
        old.setDate(date);

        log(String.format("amount = %d, balance = %d, cluster = %s, market = %s, ca = %s, catrans = %s",
            amount,
            balance, cluster.getName(), market.getName(), ca.getName(), catransfer.getName()));

        return R.REDIRECT_EDIT;
    }

    public String updateView()
    {
        updateList();
        return R.REDIRECT_VIEW;
    }

    @Override
    public List<TTransaction> updateList()
    {
        // TODO: egy ido utan ez egy kicsit lassu lesz (ha sok sora lesz)
        list = trservice.findAll(filterCa, filterCatransfer, filterCluster, filterMarket);
        log(this.getClass().getSimpleName() + "::updateList(), length = " + list.size());
        log(String.format("ca = %s, mk = %s, cl = %s", filterCa, filterMarket, filterCluster));
        return list;
    }

    /**
     * Supply additional (necessary) data to the transaction instance, which are
     * not given by the UI.
     */
    private TTransaction validate(TTransaction tr)
    {
        /* this is processed after the JSF validation was done */

        log("TRANSACTION TYPE: " + type.getName());

        if (type == TTransactionType.pivot)
        {
            tr.setPivot(true);
            tr.setAmount(0);
            tr.setCluster(ss.Szamolas());
            tr.setMarket(ss.Market_Not_Applicable());
            tr.setCatransfer(ss.CA_None());
        }
        else if (type == TTransactionType.transfer)
        {
            tr.setPivot(false);
            tr.setBalance(0);
            tr.setCluster(ss.Athelyezes());
            tr.setMarket(ss.Market_Not_Applicable());

            Assert.assertFalse("In transfer mode the catransfer shouldn't be 'none'",
                tr.getCatransfer().equals(ss.CA_None()));
        }
        else if (type == TTransactionType.simple)
        {
            tr.setPivot(false);
            tr.setBalance(0);
            tr.setCatransfer(ss.CA_None());
        }

        Assert.assertNotNull(tr.getCa());
        Assert.assertNotNull(tr.getCatransfer());

        return tr;
    }

    @Override
    public String persist()
    {
        TTransaction newtr = validate(instance());
        log("persist: %s ", newtr);

        trservice.makeTransaction(new TransactionArguments(newtr, null, R.TR_INSERTION));
        return updateView();
    }

    @Override
    public String merge()
    {
        TTransaction tr = validate(instance(uid));

        log("WILL BE UPDATED: %s", tr);

        trservice.makeTransaction(new TransactionArguments(tr, old, R.TR_UPDATE));
        return updateView();
    }

    @Override
    public String remove(TTransaction removable)
    {
        log("WILL BE REMOVED: %s", removable);

        TTransaction tr = trservice.findFirstSimpleTransactionBefore(removable);
        log("FIRST SIMPLE TRANSACTION BEFORE THE REMOVABLE: %s", tr);

        trservice.makeTransaction(new TransactionArguments(tr, removable, R.TR_REMOVAL));
        return updateView();
    }

    @Override
    public TTransaction instance()
    {
        return new TTransaction(amount, balance, date, remark, ca, cluster, market, pivot)
            .setCatransfer(catransfer);
    }

    @Override
    public TTransaction instance(int uid)
    {
        return instance().setUid(uid);
    }

    @Override
    public AbstractService<TTransaction> getService()
    {
        return trservice;
    }

    /* ============================ ROW EDIT EVENTS ============================= */

    public void onEdit(RowEditEvent event)
    {
        logger.info("onEdit(RowEditEvent)");

        AjaxBehaviorEvent evt = (AjaxBehaviorEvent) event;
        DataTable table = (DataTable) evt.getSource();
        int activeRow = table.getRowIndex();

        log(list.get(activeRow).getRemark());
        trservice.edit(list.get(activeRow));
    }

    public void onCancel(RowEditEvent event)
    {
        logger.info("onCancel(RowEditEvent)");
    }

    /* ============================ GETTERS / SETTERS ============================= */

    public TTransaction getOld()
    {
        return old;
    }

    public void setOld(TTransaction old)
    {
        this.old = old;
    }

    public Date getDate()
    {
        return date;
    }

    public void setDate(Date date)
    {
        this.date = date;
    }

    public TChargeAccount getCa()
    {
        return ca;
    }

    public void setCa(TChargeAccount ca)
    {
        this.ca = ca;
    }

    public TChargeAccount getCatransfer()
    {
        return catransfer;
    }

    public void setCatransfer(TChargeAccount catransfer)
    {
        this.catransfer = catransfer;
    }

    public int getAmount()
    {
        return amount;
    }

    public void setAmount(int amount)
    {
        this.amount = amount;
    }

    public int getBalance()
    {
        return balance;
    }

    public void setBalance(int balance)
    {
        this.balance = balance;
    }

    public TCluster getCluster()
    {
        return cluster;
    }

    public void setCluster(TCluster cluster)
    {
        this.cluster = cluster;
    }

    public TMarket getMarket()
    {
        return market;
    }

    public void setMarket(TMarket market)
    {
        this.market = market;
    }

    public String getRemark()
    {
        return remark;
    }

    public void setRemark(String remark)
    {
        this.remark = remark;
    }

    public boolean isPivot()
    {
        return pivot;
    }

    public void setPivot(boolean pivot)
    {
        this.pivot = pivot;
    }

    public TChargeAccount getFilterCa()
    {
        return filterCa;
    }

    public void setFilterCa(TChargeAccount filterCa)
    {
        this.filterCa = filterCa;
    }

    public TCluster getFilterCluster()
    {
        return filterCluster;
    }

    public void setFilterCluster(TCluster filterCluster)
    {
        this.filterCluster = filterCluster;
    }

    public TMarket getFilterMarket()
    {
        return filterMarket;
    }

    public void setFilterMarket(TMarket filterMarket)
    {
        this.filterMarket = filterMarket;
    }

    public TTransactionType getType()
    {
        return type;
    }

    public void setType(TTransactionType type)
    {
        this.type = type;
    }

    public int getID()
    {
        return ID;
    }

    public TChargeAccount getFilterCatransfer()
    {
        return filterCatransfer;
    }

    public void setFilterCatransfer(TChargeAccount filterCatransfer)
    {
        this.filterCatransfer = filterCatransfer;
    }

    // public MapEntityConverter getMecBean()
    // {
    // return mecBean;
    // }
    //
    // public void setMecBean(MapEntityConverter mecBean)
    // {
    // this.mecBean = mecBean;
    // }
}
