package polcz.budget.session.validator;

import javax.faces.validator.Validator;

import org.jboss.logging.Logger;

public abstract class AbstractValidator implements Validator
{
    Logger logger;
    
    public AbstractValidator(Class<?> childClass)
    {
        logger = Logger.getLogger("PPOLCZ_" + childClass.getSimpleName());
        logger.info(childClass.getSimpleName() + " instantiated");
    }
}
