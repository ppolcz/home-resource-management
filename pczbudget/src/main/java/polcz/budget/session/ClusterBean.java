package polcz.budget.session;

import java.io.Serializable;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import polcz.budget.model.TCluster;
import polcz.budget.service.AbstractService;
import polcz.budget.service.ClusterService;
import polcz.budget.session.helper.SimpleMapBean;

@SessionScoped
@ManagedBean(name = "clBean")
public class ClusterBean extends SimpleMapBean<TCluster, Integer, String> implements Serializable
{

    public ClusterBean()
    {
        super(ClusterBean.class);
    }

    private static final long serialVersionUID = 8237279798736467461L;

    @EJB
    private ClusterService clservice;

    private int sgn;
    private TCluster parent;

    public String create()
    {
        sgn = 0;
        return super.create();
    }

    public String edit(int uid, String name, int sgn, TCluster parent)
    {
        logger.info(String.format("uid = %d, name = %s, sng = %d, parent = %s", uid, name, sgn, parent));
        this.sgn = sgn;
        this.setParent(parent);
        return super.edit(uid, name, null /* TODO: itt nincs desc */);
    }

    public int getSgn()
    {
        return sgn;
    }

    public void setSgn(int sgn)
    {
        this.sgn = sgn;
    }

    @Override
    public TCluster instance()
    {
        logger.info("parent = " + parent);
        return new TCluster(getName(), sgn, parent);
    }

    @Override
    public TCluster instance(int uid)
    {
        return instance().setUid(uid);
    }

    @Override
    public AbstractService<TCluster> getService()
    {
        return clservice;
    }

    public TCluster getParent()
    {
        return parent;
    }

    public void setParent(TCluster parent)
    {
        this.parent = parent;
    }
}
