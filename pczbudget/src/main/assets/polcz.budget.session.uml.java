@startuml

left to right direction

hide ChargeAccountBean methods
hide ChargeAccountBean fields 
class ChargeAccountBean  << (B,orange) JSF, Session >> {
    - {static} serialVersionUID : long
    - service : CAService
    --
    + ChargeAccountBean()
    + getService() : TChargeAccount>
    + instance(int) : Object
    + instance() : Object
    + instance(int) : TChargeAccount
    + instance() : TChargeAccount
}

ChargeAccountBean --|> SimpleMapBean

hide ClusterBean methods
hide ClusterBean fields 
class ClusterBean  << (B,orange) JSF, Session >> {
    - {static} serialVersionUID : long
    - clservice : ClusterService
    - sgn : int
    - parentUid : int
    --
    + ClusterBean()
    + edit(int, String, int, int) : String
    + getService() : TCluster>
    + getParentUid() : int
    + setParentUid(int)
    + getSgn() : int
    + setSgn(int)
    + create() : String
    + instance() : Object
    + instance(int) : Object
    + instance(int) : TCluster
    + instance() : TCluster
}

ClusterBean --|> SimpleMapBean

hide MarketBean methods
hide MarketBean fields 
class MarketBean  << (B,orange) JSF, Session >> {
    - {static} serialVersionUID : long
    - service : MarketService
    --
    + MarketBean()
    + getService() : TMarket>
    + instance(int) : Object
    + instance() : Object
    + instance(int) : TMarket
    + instance() : TMarket
}

MarketBean --|> SimpleMapBean

hide TransactionBean methods
hide TransactionBean fields 
class TransactionBean  << (B,orange) JSF, Session >> {
    - {static} serialVersionUID : long
    - ID : int
    - ss : StartupService
    - trservice : TransactionService
    - caservice : CAService
    - mkservice : MarketService
    - clservice : ClusterService
    - date : Date
    - ca : TChargeAccount
    - catransfer : TChargeAccount
    - amount : int
    - balance : int
    - cluster : TCluster
    - market : TMarket
    - remark : String
    - pivot : boolean
    - type : TTransactionType
    - old : TTransaction
    - filterCa : TChargeAccount
    - filterCluster : TCluster
    - filterMarket : TMarket
    --
    + TransactionBean()
    + edit(TTransaction) : String
    + create() : String
    + persist() : String
    + merge() : String

    + onEdit(RowEditEvent)
    + onCancel(RowEditEvent)

    + updateView() : String
    - validate(TTransaction) : TTransaction
    + isPivot() : boolean
    + updateList() : TTransaction>
    + remove(Object) : String
    + remove(TTransaction) : String
}

TransactionBean --|> AbstractEntityBean

hide DefaultMappableEntity methods
hide DefaultMappableEntity fields 
abstract class DefaultMappableEntity {
}

MapEntityConverter o.. MarketBean
MapEntityConverter o.. ClusterBean
MapEntityConverter o.. ChargeAccountBean

hide MapEntityConverter methods
hide MapEntityConverter fields 
MapEntityConverter -l- DefaultMappableEntity
class MapEntityConverter << (B,orange) JSF, Request >> {
}

'hide AbstractEntityBean methods
'hide AbstractEntityBean fields 
class AbstractEntityBean {
    # list : List
    # uid : int
    --
    + init() //PostConstruct//
    + updateList() : List<T>
    + persist() : String
    + merge() : String
    + remove(T) : String
}

AbstractEntityBean ..|> EntityBeanHelperMethods

hide EntityBeanHelperMethods fields
hide EntityBeanHelperMethods methods
interface EntityBeanHelperMethods {
    --
    + {abstract} getService() : S
    + {abstract} getList() : List<T>
    + {abstract} instance(int) : T
    + {abstract} instance() : T
}


hide SimpleMapBean methods
hide SimpleMapBean fields 
class SimpleMapBean {
    - {static} serialVersionUID : long
    - name : String
    - desc : String
    ~ items : Map
    ~ itemsNew : Map
    --
    + getItems()
    + SimpleMapBean(Class<?>)
    + getDesc() : String
    + edit(int, String, String) : String
    + setDesc(String)
    + getItemsNew() : T>
    + getName() : String
    + create() : String
    + setName(String)
}

SimpleMapBean --|> AbstractEntityBean

'hide AbstractValidator methods
'hide AbstractValidator fields 

hide TransactionValidator methods
hide TransactionValidator fields 
class TransactionValidator << (B,orange) JSF, Request >> {
    ~ ss : StartupService
    - cafrom : TChargeAccount
    - cafromInput : UIInput
    --
    + TransactionValidator()
    + validate(FacesContext, UIComponent, Object)
    - sendMessage(UIInput, boolean, FacesMessage)
    + validateCluster(FacesContext, UIComponent, Object)
    + validateMarket(FacesContext, UIComponent, Object)
    + validateChargeAccountTo(FacesContext, UIComponent, Object)
    + validateChargeAccountFrom(FacesContext, UIComponent, Object)
    - validateChargeAccount(FacesContext, UIInput, Object, boolean)
}

'TransactionValidator --|> AbstractValidator

TransactionBean -l- TransactionValidator


'hide DefaultMappableEntity methods
'hide DefaultMappableEntity fields 
'class DefaultMappableEntity {
'    - {static} serialVersionUID : long
'    --
'    + DefaultMappableEntity()
'    + equals(Object) : boolean
'    + toString() : String
'    + hashCode() : int
'}
'
'DefaultMappableEntity ..|> MappableEntity
'
'hide MappableEntity fields
'hide MappableEntity methods
'interface MappableEntity {
'    --
'    + {abstract} setKey(K) : V>
'    + {abstract} getValue() : V
'    + {abstract} getKey() : K
'    + {abstract} setValue(V) : V>
'}

@enduml
