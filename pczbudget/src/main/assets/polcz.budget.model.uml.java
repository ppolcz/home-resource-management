@startuml

left to right direction

class DefaultMappableEntity {
    - {static} serialVersionUID : long
    --
    + DefaultMappableEntity()
    + equals(Object) : boolean
    + toString() : String
    + hashCode() : int
}

DefaultMappableEntity ..|> MappableEntity

interface MappableEntity {
    --
    + {abstract} setKey(K) : V>
    + {abstract} getValue() : V
    + {abstract} getKey() : K
    + {abstract} setValue(V) : V>
}

hide TChargeAccount methods
hide TCluster methods
hide TMarket methods
hide TTransaction methods
hide TTransactionType methods

class TChargeAccount {
    - {static} serialVersionUID : long
    - uid : int
    - desc : String
    - name : String
    - TTransactions : List
}

TChargeAccount --|> DefaultMappableEntity

class TCluster {
    - {static} serialVersionUID : long
    - uid : int
    - name : String
    - sgn : int
    - parent : TCluster
    - children : List
    - pis : List
    - trs : List
}

TCluster --|> DefaultMappableEntity

class TMarket {
    - {static} serialVersionUID : long
    - uid : int
    - desc : String
    - name : String
    - TProductInfos : List
    - trs : List
}

TMarket --|> DefaultMappableEntity

class TTransaction {
    - {static} serialVersionUID : long
    - uid : int
    - amount : int
    - balance : int
    - date : Date
    - remark : String
    - pivot : boolean
    - pis : List
    - ca : TChargeAccount
    - catransfer : TChargeAccount
    - cluster : TCluster
    - market : TMarket
}


enum TTransactionType {
    simple
    transfer
    pivot
}

@enduml