@startuml

class ChargeAccountBean {
    - {static} serialVersionUID : long
    - service : CAService
    --
    + ChargeAccountBean()
    + getService() : TChargeAccount>
    + instance(int) : Object
    + instance() : Object
    + instance(int) : TChargeAccount
    + instance() : TChargeAccount
}

ChargeAccountBean ..|> Serializable
ChargeAccountBean --|> SimpleMapBean

class ClusterBean {
    - {static} serialVersionUID : long
    - clservice : ClusterService
    - sgn : int
    - parentUid : int
    --
    + ClusterBean()
    + edit(int, String, int, int) : String
    + getService() : TCluster>
    + getParentUid() : int
    + setParentUid(int) : void
    + getSgn() : int
    + setSgn(int) : void
    + create() : String
    + instance() : Object
    + instance(int) : Object
    + instance(int) : TCluster
    + instance() : TCluster
}

ClusterBean ..|> Serializable
ClusterBean --|> SimpleMapBean

class MarketBean {
    - {static} serialVersionUID : long
    - service : MarketService
    --
    + MarketBean()
    + getService() : TMarket>
    + instance(int) : Object
    + instance() : Object
    + instance(int) : TMarket
    + instance() : TMarket
}

MarketBean ..|> Serializable
MarketBean --|> SimpleMapBean

class TransactionBean {
    - {static} serialVersionUID : long
    - ID : int
    - ss : StartupService
    - trservice : TransactionService
    - caservice : CAService
    - mkservice : MarketService
    - clservice : ClusterService
    - date : Date
    - ca : TChargeAccount
    - catransfer : TChargeAccount
    - amount : int
    - balance : int
    - cluster : TCluster
    - market : TMarket
    - remark : String
    - pivot : boolean
    - type : TTransactionType
    - old : TTransaction
    - filterCa : TChargeAccount
    - filterCluster : TCluster
    - filterMarket : TMarket
    --
    + TransactionBean()
    + getDate() : Date
    + edit(TTransaction) : String
    + getService() : TTransaction>
    + onEdit(RowEditEvent) : void
    + onCancel(RowEditEvent) : void
    + getOld() : TTransaction
    + setOld(TTransaction) : void
    + setRemark(String) : void
    + getFilterCa() : TChargeAccount
    + setFilterCa(TChargeAccount) : void
    + getFilterCluster() : TCluster
    + setFilterCluster(TCluster) : void
    + getFilterMarket() : TMarket
    + setFilterMarket(TMarket) : void
    + setType(TTransactionType) : void
    + setCa(TChargeAccount) : void
    + setCatransfer(TChargeAccount) : void
    + setDate(Date) : void
    + updateView() : String
    - validate(TTransaction) : TTransaction
    + setPivot(boolean) : void
    + setAmount(int) : void
    + setCluster(TCluster) : void
    + setMarket(TMarket) : void
    + setBalance(int) : void
    + getTypes() : TTransactionType;
    + getAmount() : int
    + getBalance() : int
    + getRemark() : String
    + isPivot() : boolean
    + getCa() : TChargeAccount
    + getCatransfer() : TChargeAccount
    + getCluster() : TCluster
    + getMarket() : TMarket
    + persist() : String
    + updateList() : TTransaction>
    + remove(Object) : String
    + remove(TTransaction) : String
    + init() : void
    + create() : String
    + merge() : String
    + getType() : TTransactionType
    + getID() : int
    + instance() : TTransaction
    + instance() : Object
    + instance(int) : Object
    + instance(int) : TTransaction
}

TransactionBean ..|> Serializable
TransactionBean --|> AbstractEntityBean

class MapEntityConverter {
    ~ logger : Logger
    - mkBean : MarketBean
    - clBean : ClusterBean
    - caBean : ChargeAccountBean
    --
    + MapEntityConverter()
    + getAsObject(TChargeAccount) : TChargeAccount
    + getAsObject(TMarket) : TMarket
    + getAsObject(FacesContext, UIComponent, String) : Object
    + getAsObject(TCluster) : TCluster
    + getCaBean() : ChargeAccountBean
    + getAsString(FacesContext, UIComponent, Object) : String
    + getMkBean() : MarketBean
    + setMkBean(MarketBean) : void
    + getClBean() : ClusterBean
    + setClBean(ClusterBean) : void
    + setCaBean(ChargeAccountBean) : void
    - init() : void
}

MapEntityConverter ..|> Converter

class AbstractEntityBean {
    + {static} NULL_VALUE : int
    - ID : int
    # list : List
    # uid : int
    # logger : Logger
    --
    + setUid(int) : void
    + getList() : List<T>
    + persist() : String
    + updateList() : List<T>
    + getUid() : int
    + getNullValue() : int
    + preRender(ComponentSystemEvent) : void
    + change(ValueChangeEvent) : void
    + ajaxListener(AjaxBehaviorEvent) : void
    + actionListener(ActionEvent) : void
    + submit() : String
    + remove(T) : String
    # log(String, Object[]) : void
    + init() : void
    + merge() : String
    + getID() : int
}

AbstractEntityBean ..|> EntityBeanHelperMethods

interface EntityBeanHelperMethods {
    --
    + {abstract} getService() : S
    + {abstract} getList() : List<T>
    + {abstract} instance(int) : T
    + {abstract} instance() : T
}


class SimpleMapBean {
    - {static} serialVersionUID : long
    - name : String
    - desc : String
    ~ items : Map
    ~ itemsNew : Map
    --
    + SimpleMapBean(Class<?>)
    + getDesc() : String
    + edit(int, String, String) : String
    + setDesc(String) : void
    + getItems() : K>
    + getItemsNew() : T>
    + getName() : String
    + create() : String
    + setName(String) : void
}

SimpleMapBean ..|> Serializable
SimpleMapBean --|> AbstractEntityBean

class AbstractValidator {
    ~ logger : Logger
    --
    + AbstractValidator(Class<?>)
}

AbstractValidator ..|> Validator

class TransactionValidator {
    ~ ss : StartupService
    - cafrom : TChargeAccount
    - cafromInput : UIInput
    --
    + TransactionValidator()
    + validate(FacesContext, UIComponent, Object) : void
    - sendMessage(UIInput, boolean, FacesMessage) : void
    + validateCluster(FacesContext, UIComponent, Object) : void
    + validateMarket(FacesContext, UIComponent, Object) : void
    + validateChargeAccountTo(FacesContext, UIComponent, Object) : void
    + validateChargeAccountFrom(FacesContext, UIComponent, Object) : void
    - validateChargeAccount(FacesContext, UIInput, Object, boolean) : void
}

TransactionValidator --|> AbstractValidator

class DefaultMappableEntity {
    - {static} serialVersionUID : long
    --
    + DefaultMappableEntity()
    + equals(Object) : boolean
    + toString() : String
    + hashCode() : int
}

DefaultMappableEntity ..|> MappableEntity
DefaultMappableEntity ..|> Serializable

interface MappableEntity {
    --
    + {abstract} setKey(K) : V>
    + {abstract} getValue() : V
    + {abstract} getKey() : K
    + {abstract} setValue(V) : V>
}


class TChargeAccount {
    - {static} serialVersionUID : long
    - uid : int
    - desc : String
    - name : String
    - TTransactions : List
    --
    + TChargeAccount()
    + TChargeAccount(String)
    + TChargeAccount(String, String)
    + getDesc() : String
    + setUid(int) : TChargeAccount
    + setKey(Object) : MappableEntity
    + setKey(Integer) : String>
    + setDesc(String) : void
    + getUid() : int
    + getTTransactions() : TTransaction>
    + setTTransactions(List<TTransaction>) : void
    + addTTransaction(TTransaction) : TTransaction
    + removeTTransaction(TTransaction) : TTransaction
    + equals(TChargeAccount) : boolean
    + getName() : String
    + getValue() : Object
    + getValue() : String
    + getKey() : Object
    + getKey() : Integer
    + setName(String) : TChargeAccount
    + setValue(Object) : MappableEntity
    + setValue(String) : String>
}

TChargeAccount --|> DefaultMappableEntity

class TCluster {
    - {static} serialVersionUID : long
    - uid : int
    - name : String
    - sgn : int
    - parent : TCluster
    - children : List
    - pis : List
    - trs : List
    --
    + TCluster(String, int, TCluster)
    + TCluster(String, TCluster)
    + TCluster(String)
    + TCluster()
    + setUid(int) : TCluster
    + setKey(Object) : MappableEntity
    + setKey(Integer) : String>
    + getUid() : int
    + getSgn() : int
    + setSgn(int) : TCluster
    + getTTransactions() : TTransaction>
    + setTTransactions(List<TTransaction>) : void
    + addTTransaction(TTransaction) : TTransaction
    + removeTTransaction(TTransaction) : TTransaction
    + getTClusters() : TCluster>
    + setChildren(List<TCluster>) : void
    + addChild(TCluster) : TCluster
    + removeChild(TCluster) : TCluster
    + getTProductInfos() : TProductInfo>
    + setTProductInfos(List<TProductInfo>) : void
    + addTProductInfo(TProductInfo) : TProductInfo
    + removeTProductInfo(TProductInfo) : TProductInfo
    + equals(TCluster) : boolean
    + getName() : String
    + getValue() : String
    + getValue() : Object
    + getKey() : Object
    + getKey() : Integer
    + getParent() : TCluster
    + setName(String) : TCluster
    + setValue(Object) : MappableEntity
    + setValue(String) : String>
    + setParent(TCluster) : TCluster
}

TCluster --|> DefaultMappableEntity

class TMarket {
    - {static} serialVersionUID : long
    - uid : int
    - desc : String
    - name : String
    - TProductInfos : List
    - trs : List
    --
    + TMarket(String, String)
    + TMarket()
    + getDesc() : String
    + setUid(int) : TMarket
    + setKey(Object) : MappableEntity
    + setKey(Integer) : String>
    + setDesc(String) : void
    + getUid() : int
    + getTTransactions() : TTransaction>
    + setTTransactions(List<TTransaction>) : void
    + addTTransaction(TTransaction) : TTransaction
    + removeTTransaction(TTransaction) : TTransaction
    + getTProductInfos() : TProductInfo>
    + setTProductInfos(List<TProductInfo>) : void
    + addTProductInfo(TProductInfo) : TProductInfo
    + removeTProductInfo(TProductInfo) : TProductInfo
    + getName() : String
    + getValue() : Object
    + getValue() : String
    + getKey() : Object
    + getKey() : Integer
    + setName(String) : TMarket
    + setValue(Object) : MappableEntity
    + setValue(String) : String>
}

TMarket --|> DefaultMappableEntity

class TTransaction {
    - {static} serialVersionUID : long
    - uid : int
    - amount : int
    - balance : int
    - date : Date
    - remark : String
    - pivot : boolean
    - pis : List
    - ca : TChargeAccount
    - catransfer : TChargeAccount
    - cluster : TCluster
    - market : TMarket
    --
    + TTransaction(int, int, Date, String, TChargeAccount, TCluster, TMarket, boolean)
    + TTransaction()
    + getDate() : Date
    + setUid(int) : TTransaction
    + setRemark(String) : void
    + setCa(TChargeAccount) : void
    + setCatransfer(TChargeAccount) : TTransaction
    + setDate(Date) : void
    + setPivot(boolean) : void
    + setAmount(int) : void
    + setCluster(TCluster) : void
    + setMarket(TMarket) : void
    + setBalance(int) : void
    + getAmount() : int
    + getBalance() : int
    + getRemark() : String
    + isPivot() : boolean
    + getCa() : TChargeAccount
    + getCatransfer() : TChargeAccount
    + getCluster() : TCluster
    + getMarket() : TMarket
    + getUid() : int
    + getTProductInfos() : TProductInfo>
    + setTProductInfos(List<TProductInfo>) : void
    + addTProductInfo(TProductInfo) : TProductInfo
    + removeTProductInfo(TProductInfo) : TProductInfo
    + toString() : String
}

TTransaction ..|> Serializable

enum TTransactionType {
    simple
    transfer
    pivot
    - {static} ENUM$VALUES : TTransactionType;
    --
    + {static} values() : TTransactionType;
    + {static} valueOf(String) : TTransactionType
    + getName() : String
}


class AbstractService {
    - entityClass : Class
    # logger : Logger
    --
    + AbstractService(Class<T>)
    + edit(T) : void
    + findAll() : List<T>
    # {abstract} em() : EntityManager
    + findRange([I) : List<T>
    + remove(T) : void
    + count() : int
    + find(Object) : T
    + create(T) : void
}


class CAService {
    - em : EntityManager
    --
    + CAService()
    + edit(Object) : void
    + edit(TChargeAccount) : void
    # em() : EntityManager
    + remove(TChargeAccount) : void
    + remove(Object) : void
    + create(Object) : void
    + create(TChargeAccount) : void
}

CAService --|> AbstractService

class ClusterService {
    - em : EntityManager
    --
    + ClusterService()
    # em() : EntityManager
}

ClusterService --|> AbstractService

class MarketService {
    - em : EntityManager
    --
    + MarketService()
    # em() : EntityManager
}

MarketService --|> AbstractService

class StartupService {
    ~ em : EntityManager
    ~ logger : Logger
    - Nem_Adott : TCluster
    - Napi_Szukseglet : TCluster
    - Szamolas : TCluster
    - Athelyezes : TCluster
    - Market_Not_Applicable : TMarket
    - CA_None : TChargeAccount
    - pkez : TChargeAccount
    --
    + StartupService()
    + Szamolas() : TCluster
    + Athelyezes() : TCluster
    - ca(TChargeAccount) : TChargeAccount
    - cluster(TCluster) : TCluster
    - market(TMarket) : TMarket
    + CA_None() : TChargeAccount
    + pkez() : TChargeAccount
    + Napi_Szukseglet() : TCluster
    + Market_Not_Applicable() : TMarket
    + Nem_Adott() : TCluster
    - findOrCreate(Class<T>, T) : T
    - generateClusters() : void
    + init() : void
}


class TransactionService {
    - em : EntityManager
    ~ ss : StartupService
    + {static} REMOVAL : int
    + {static} INSERTION : int
    + {static} UPDATE : int
    - utx : UserTransaction
    --
    + TransactionService()
    + edit(Object) : void
    + edit(TTransaction) : void
    + findFirstSimpleTransactionBefore(TTransaction) : TTransaction
    + makeTransaction(TransactionArguments) : boolean
    + findAll(TChargeAccount, TCluster, TMarket) : TTransaction>
    # em() : EntityManager
    - executeTransaction(TransactionArguments) : void
    - makeRollback() : boolean
    - findSingleOrNull(TypedQuery<T>) : T
    + findLastPivotBefore(TTransaction) : TTransaction
    + findLastPivotBefore(Date, TChargeAccount) : TTransaction
    + findFirstPivotAfter(TTransaction) : TTransaction
    + findFirstPivotAfter(Date, TChargeAccount) : TTransaction
    + findElementsBetween(Date, Date, TChargeAccount) : TTransaction>
    - find(TransactionService$SelectByArguments) : TTransaction>
}

TransactionService --|> AbstractService

class TransactionArguments {
    + {static} REMOVAL : int
    + {static} INSERTION : int
    + {static} UPDATE : int
    - acttr : TTransaction
    - oldtr : TTransaction
    - type : int
    --
    + TransactionArguments(TTransaction, TTransaction, int)
    + setType(int) : void
    + validate() : void
    + setActtr(TTransaction) : void
    + getActtr() : TTransaction
    + getOldtr() : TTransaction
    + setOldtr(TTransaction) : void
    + getType() : int
}


class TTransactionUpdater {
    - none : TChargeAccount
    - athelyezes : TCluster
    - logger : Logger
    - service : TransactionService
    - args : TransactionArguments
    --
    + TTransactionUpdater(TransactionService, Logger, TransactionArguments, TCluster, TChargeAccount)
    - recalculate(List<TTransaction>) : TTransactionUpdater
    - findDirtyElements(Date, Date, TChargeAccount) : TTransaction>
    - updateDirtyElements(Date, Date, TChargeAccount) : void
    # log(TTransaction) : void
    # log(String, Object[]) : void
    + execute() : void
}

@enduml