export JAVA_HOME="/usr/lib/jvm/java-8-openjdk-amd64/"

# create project
# http://maven.apache.org/guides/getting-started/maven-in-five-minutes.html
mvn archetype:generate \
    -DgroupId=com.polpe.hrm.odf \
    -DartifactId=odf-parser \
    -DarchetypeArtifactId=maven-archetype-quickstart \
    -DinteractiveMode=false

mvn dependency:tree
mvn dependency:copy-dependencies
mvn eclipse:eclipse

mvn package

java -cp "target/odf-parser-1.0-SNAPSHOT.jar:target/dependency/*" com.polpe.hrm.odf.parser.App  
