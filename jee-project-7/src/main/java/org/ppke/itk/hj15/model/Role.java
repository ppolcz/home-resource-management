package org.ppke.itk.hj15.model;

public enum Role {
	
	ADMIN,
	GUEST,
	CUSTOMER;

}
